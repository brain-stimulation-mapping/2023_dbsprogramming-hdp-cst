# Pipeline for manuscript

## **About the project**
Diffusion MRI preprocessing and analysis for generating streamline-based pathway activation models of the hyperdirect pathway and corticospinal tract.

## **Requirements**

**Software**
- MRtrix3
- FSL
- synb0
- Freesurfer
- LeadDBS

**Files**
- fs_LUT folder
/home/alba/03_Data/fs_LUT/ BA_exth_original.txt & BA_exth_ordered.txt

- MNI_fsl folder
/home/alba/03_Data/MNI_fsl/

- Atlas folder
/home/alba/03_Data/ATLAS



## **MRI data**

Convert DICOM files and save data in the respective folders (see folder structure below).

- dwi_raw.mif
- t1.nii.gz
- t2.nii.gz

```
Note:
.mif format is used by MRtrix and keeps info about bvals and bvecs.
.nii or .nii.gz is the format used but FSL and many other imaging softwares
```

## **Folder structure**

```
Project
|-- 00_eddy*
|-- 00_synb0*
|-- subjects
      |-- subjectID
            |-- atlasRegistration_fsl
            |-- atlasRegistration_lead
            |
            |-- DWI*
            |      |-- 00_synb0
            |      |-- 00_eddy
            |      |-- dwi_raw.mif
            |
            |-- fs_parcellation
            |
            |-- STIMULATIONS?????????????????
            |
            |-- T1*
            |    |-- t1.nii.gz
            |
            |-- T2*
            |    |-- t2.nii.gz
            |
            |-- tractography

```

*folder manually created with contents tailored for datatype


## **Code**

### 01_Preproc

Scripts for pre-processing dMRI data, calculate image transformations, obtain whole-brain tractogram and pathways of interest and registration of Volumes of Tissue Activated.

Xa) scripts for individual analysis
Xb) scripts for group analysis



#### FREESURFER

**1. _fs_parcel.sh_**

Freesurfer recon-all parcellation will be used at different stages of the pipeline (7). Depending on the machine and subject, execution time will be 4 to xxxx hours.
It is recommended to execute this command in the beginning to save some time.

The rest of the scrip, were Brodmann areas are co-registered to diffusion space has to be executed after obtaining the diff2struct transforms in (4).

```
# If only T1 available
recon-all -s $subjectID -i T1/t1.nii.gz -all -parallel

## If T2 available
recon-all -s $subjectID -i T1/t1.nii.gz -T2 T2/t2.nii.gz -all -parallel
```

#### PREPROCESSING

**2. _PreprocDQS_1.sh_**

Preprocessing DQS scans without reversed PE (part 1).
Denoising, unring, synb0 and prepare eddy.


**3. Preproc_DQS_2**

**3a. _PreprocDQS_2_individual.sh_**

Preprocessing DQS scans without reversed PE (part 2) for an individual subject.
Runs eddy, calculates DWI mask, estimates response function, obtains fiber orientation distribution, and performs normalization.

**3b. (1) _PreprocDQS_2_group.sh_**

Preprocessing DQS scans without reversed PE (part 2) for a group of subjects.
Runs eddy and calculates DWI mask.

**3b. (2) _group_avg_response.sh_**

Script for group analysis (for inter-subject comparison).
Response function, group average response function, DWI masks, fiber orientation distribution, intensity normalization


#### IMAGE TRANSFORMATIONS

**4. _diff2struct.sh_**

Coregisters b0 to T1 & T2 using flirt and bbr.
Obtains transformation files and coregisters T1 & T2 to diffusion space.

**5. _mni2struct.sh_**

Coregisters T1 to FSL-MNI space with ANTS.

**6. (1) _LeadDBS_transforms.sh_**

Converts LeadDBS ants transform (MNI-LeadDBS to anat_T1) for usage in MRtrix.
Obtains transform anat_t1 to diffusion.

**6. (2) _LeadDBS_transformsT2.sh_**

Converts LeadDBS ants transform (MNI-LeadDBS to anat_T2) for usage in MRtrix.
Obtains transform anat_t2 to diffusion.

TODO: check correct execution


#### WHOLE-BRAIN TRACTOGRAPHY

**7. ACT_tracking**

**7a. _ACT_tracking.sh_**

Script for anatomically-constrained tractography (ACT), whole-brain tractography and SIFT2.

**7b. _ACT_tracking_group.sh_**

Script for anatomically-constrained tractography (ACT), whole-brain tractography and SIFT2. Use with group avereage response functions.


#### PATHWAY TRACTOGRAPHY

**8. _HDP_tracking.sh_**
Obtain hyperdirect pathway (HDP) from whole-brain tractogram. 

**9. _CST_tracking.sh_** Obtain corticospinal tract (CST) from whole-brain tractogram

(modified to include/exclude CST & ML ROIs as in CSTtracking5.sh)

This script was cleaned a posterior of the analysis. In the original work CSTtracking.sh & CSTtracking5.sh were used.


### **02_Lead_VTA_Recon**
Lead reconstruction, VTA generation and co-registration to patient space.

DBS Lead were localized for each patient with Lead DBS version X.

**21. Massive VTA generation**

(Ask Khoa for Matlab script)

**22. _VTAregister_massive.sh_** Co-register and calculate streamlines inside VTAs for a group of subjects (massive VTA calculation).


### **03_Analysis**
Scripts for generating pathway activation models and data analysis

**31. _total_sum_w.py_** Create dataframes containing sum of weights (HDP and CST) for a group of subjects.

**32. _FBC_massive.py_**

Group script to create, for each subject, dataframes for effect and side_effect containing the following data:

['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'total_sum_w', 'percent_w', 'fbc', 'path']

**32. _clinical_mA.ipynb_** Get mapping data from excel sheets.

**33. _getVTAs.ipynb_** Get VTAs for training the models.

**33. _data_analysis_model_LOLO_xxx.ipynb_** Train logistic regression models.

**34. _best_level_contact.ipynb_** Select best and worst levels and contacts.

**35. _best_level_contact_2.ipynb_** Select best level and contact using both pathways, HDP and CST, instead of one of each individually.

**36. _best_window.ipynb_** Suggest best level & contact using window

**37. _best_threshold.ipynb_** Obtain stimulation threshold suggestion errors

**38. _perm_test_x_accScore.ipynb_** Permutation test for significance of level/contact suggestions