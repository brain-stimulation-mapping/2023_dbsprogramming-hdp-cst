# Create dataframes comntaining sum of weights (HDP and CST) for all subjects
# ['LeadID' 'total_sum_w']

from pathlib import Path
from collections import namedtuple
import numpy as np
import pandas as pd


rootdir = '/home/alba/2021_HDP/03_Data/subjects/'
p = Path(rootdir)

File_HDP = namedtuple('File', 'name total_sum_w path')
File_ind = namedtuple('File', 'name total_sum_w path')
File_CST = namedtuple('File', 'name total_sum_w path')

#Create empty list
files_HDP = []
files_ind = []
files_CST = []

# loop in "subjects" folder to repeat process for each subject

# LOAD DATA FROM .csv FILES
for item in sorted(p.glob('**/tractography/00_HDP/*_HDP_weights.csv')):
    name = item.name
    path = Path.resolve(item).parent
    weights = np.genfromtxt(item, delimiter=' ')
    sum_weights = np.sum(weights)
    files_HDP.append(File_HDP(name, sum_weights, path))

df_HDP = pd.DataFrame(files_HDP)
#Create LeadID column with PatientID and hemisphere
df_HDP['LeadID'] = pd.DataFrame((df_HDP.path.apply(str).str.split('/').str[6]) + '_' + df_HDP.name.str.split('_').str[0])
df_HDP = df_HDP[['LeadID', 'total_sum_w']]

for item in sorted(p.glob('**/tractography/00_ind/*_ind_weights.csv')):
    name = item.name
    path = Path.resolve(item).parent
    weights = np.genfromtxt(item, delimiter=' ')
    sum_weights = np.sum(weights)
    files_ind.append(File_ind(name, sum_weights, path))

df_ind = pd.DataFrame(files_ind)
#Create LeadID column with PatientID and hemisphere
df_ind['LeadID'] = pd.DataFrame((df_ind.path.apply(str).str.split('/').str[6]) + '_' + df_ind.name.str.split('_').str[0])
df_ind = df_ind[['LeadID', 'total_sum_w']]

for item in sorted(p.glob('**/tractography/00_CST/*CST_weights.csv')):
    name = item.name
    path = Path.resolve(item).parent
    weights = np.genfromtxt(item, delimiter=' ')
    sum_weights = np.sum(weights)
    files_CST.append(File_CST(name, sum_weights, path))

df_CST = pd.DataFrame(files_CST)
df_CST['LeadID'] = pd.DataFrame((df_CST.path.apply(str).str.split('/').str[6]) + '_' + df_CST.name.str.split('_').str[0])
df_CST = df_CST[['LeadID', 'total_sum_w']]

# Save data
dir_save = '/home/alba/2021_HDP/03_Data/FBCcalc_simbio_fastfield_v2/'
df_HDP.to_csv(dir_save + 'df_total_sum_w_HDP.csv', index=False)
df_ind.to_csv(dir_save + 'df_total_sum_w_ind.csv', index=False)
df_CST.to_csv(dir_save + 'df_total_sum_w_CST.csv', index=False)