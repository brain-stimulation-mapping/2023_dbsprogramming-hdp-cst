# Create and save dataframes containg data for ALL subjects: effect/no_effect --- side_effect/no_side_effect
# Save data in "FBCcalc_simbio_fastfield_v2"
# df_effect.csv, df_no_effect.csv, df_side_effect.csv, df_no_side_effect.csv

from pathlib import Path
import pandas as pd

# Create empty dataframes
data_HDP = pd.DataFrame(columns=['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path'])
data_HDP_ne = pd.DataFrame(columns=['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path'])

data_ind = pd.DataFrame(columns=['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path'])
data_ind_ne = pd.DataFrame(columns=['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path'])

data_CST = pd.DataFrame(columns=['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path'])
data_CST_ne = pd.DataFrame(columns=['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path'])


# loop in "subjects" folder to repeat process for each subject

rootdir = '/home/alba/2021_HDP/03_Data/subjects/'
entries = Path(rootdir)

for entry in sorted(entries.iterdir()): # each entry is one of the subjects' subfolder
    #print(entry.name)
    # LOAD DATA FROM .csv FILES

    for file in entry.glob('**/df_HDP.csv'):
        df_HDP = pd.read_csv(file) # dataframe effect

    for file in entry.glob('**/df_ne_HDP.csv'):
        df_ne_HDP = pd.read_csv(file) # dataframe ne_effect
        df_ne_HDP = df_ne_HDP.replace('ne', 'e', regex=True) # replace name of contacts so they match strings in df_effect

    for file in entry.glob('**/df_ind.csv'):
        df_ind = pd.read_csv(file)  # dataframe effect

    for file in entry.glob('**/df_ne_ind.csv'):
        df_ne_ind = pd.read_csv(file)  # dataframe ne_effect
        df_ne_ind = df_ne_ind.replace('ne', 'e', regex=True)  # replace name of contacts so they match strings in df_effect

    for file in entry.glob('**/df_CST.csv'):
        df_CST = pd.read_csv(file)  # df side_effect

    for file in entry.glob('**/df_ne_CST.csv'):
        df_ne_CST = pd.read_csv(file)  # df ne_side_effect
        df_ne_CST = df_ne_CST.replace('ne', 'se', regex=True)  # replace name of contacts so they match strings in df_side_effect

    # HDP
    # Obtain FCB values of ne_effect contacts which are present in effect
    df_HDP_cn = df_HDP[['contact', 'name']] # df containing only values of 'contact' & 'name'
    keys = list(df_HDP_cn.columns.values)
    i_HDP = df_HDP_cn.set_index(keys).index
    i_ne_HDP = df_ne_HDP.set_index(keys).index
    df_ne_HDP_filtered = df_ne_HDP[i_ne_HDP.isin(i_HDP)] # df ne_HDP with filtered contacts

    # indirect
    # Obtain FCB values of ne_effect contacts which are present in effect
    df_ind_cn = df_ind[['contact', 'name']]  # df containing only values of 'contact' & 'name'
    keys = list(df_ind_cn.columns.values)
    i_ind = df_ind_cn.set_index(keys).index
    i_ne_ind = df_ne_ind.set_index(keys).index
    df_ne_ind_filtered = df_ne_ind[i_ne_ind.isin(i_ind)]  # df ne_ind with filtered contacts

    # CST
    # Obtain FCB values of ne_effect contacts which are present in effect
    df_CST_cn = df_CST[['contact', 'name']]  # df containing only values of 'contact' & 'name'
    keys = list(df_CST_cn.columns.values)
    i_CST = df_CST_cn.set_index(keys).index
    i_ne_CST = df_ne_CST.set_index(keys).index
    df_ne_CST_filtered = df_ne_CST[i_ne_CST.isin(i_CST)]  # df ne_ind with filtered contacts


    # Append obtained fbc values to dataframes
    data_HDP = data_HDP.append(df_HDP[['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']], ignore_index=True)
    data_HDP_ne = data_HDP_ne.append(df_ne_HDP_filtered[['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']], ignore_index=True)

    data_ind = data_ind.append(df_ind[['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']], ignore_index=True)
    data_ind_ne = data_ind_ne.append(df_ne_ind_filtered[['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']], ignore_index=True)

    data_CST = data_CST.append(df_CST[['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']], ignore_index=True)
    data_CST_ne = data_CST_ne.append(df_ne_CST_filtered[['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']], ignore_index=True)


# Save data
dir_save = '/home/alba/2021_HDP/03_Data/FBCcalc_simbio_fastfield_v2/'
data_HDP.to_csv(dir_save + 'data_HDP.csv', index=False)
data_HDP_ne.to_csv(dir_save + 'data_HDP_ne.csv', index=False)

data_ind.to_csv(dir_save + 'data_ind.csv', index=False)
data_ind_ne.to_csv(dir_save + 'data_ind_ne.csv', index=False)

data_CST.to_csv(dir_save + 'data_CST.csv', index=False)
data_CST_ne.to_csv(dir_save + 'data_CST_ne.csv', index=False)