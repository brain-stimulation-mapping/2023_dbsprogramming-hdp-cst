#!/bin/bash
# Obtain HDP and indirect pathway from WB tractogram
# Use dilate ROIs
# Atlas ROI's already co-registered to diffusion space in HDPtracking.sh

path="/home/alba/2021_HDP/03_Data/subjects"
cd $path

for_each * : mkdir IN/tractography/00_ind
#for_each * : mkdir IN/tractography/00_HDP

# Dilate ROI's
#for_each */fs_parcellation/brodmann : maskfilter -npass 1 IN/L_BA4_BA6.mif dilate IN/L_BA4_BA6_dilate.mif
#for_each */fs_parcellation/brodmann : maskfilter -npass 1 IN/R_BA4_BA6.mif dilate IN/R_BA4_BA6_dilate.mif
#for_each */atlasRegistration_lead/00_Distal/* : maskfilter -npass 1 IN/GPe.nii.gz dilate IN/GPe_dilate.nii.gz
#for_each */atlasRegistration_lead/00_Distal/* : maskfilter -npass 1 IN/GPi.nii.gz dilate IN/GPi_dilate.nii.gz
for_each */atlasRegistration_fsl/00_Striatum : maskfilter -npass 1 IN/striatum_coreg.nii.gz dilate IN/striatum_coreg_dilate.nii.gz

# Indirect pathway
for_each * : tckedit IN/tractography/tracks_10mio.tck IN/tractography/00_ind/L_ind.tck -maxlength 90 \
-tck_weights_in IN/tractography/sift2_weights_10mio.csv -tck_weights_out IN/tractography/00_ind/L_ind_weights.csv \
-include IN/fs_parcellation/brodmann/L_BA4_BA6_dilate.mif -include IN/atlasRegistration_fsl/00_Striatum/striatum_coreg_dilate.nii.gz \
-include IN/atlasRegistration_lead/00_Distal/lh/GPe_dilate.nii.gz -include IN/atlasRegistration_lead/00_Distal/lh/STN.nii.gz \
-exclude IN/atlasRegistration_lead/00_Distal/lh/GPi.nii.gz

for_each * : tckedit IN/tractography/tracks_10mio.tck IN/tractography/00_ind/R_ind.tck -maxlength 90 \
-tck_weights_in IN/tractography/sift2_weights_10mio.csv -tck_weights_out IN/tractography/00_ind/R_ind_weights.csv \
-include IN/fs_parcellation/brodmann/R_BA4_BA6_dilate.mif -include IN/atlasRegistration_fsl/00_Striatum/striatum_coreg_dilate.nii.gz \
-include IN/atlasRegistration_lead/00_Distal/rh/GPe_dilate.nii.gz -include IN/atlasRegistration_lead/00_Distal/rh/STN.nii.gz \
-exclude IN/atlasRegistration_lead/00_Distal/rh/GPi.nii.gz

# HDP
for_each * : tckedit IN/tractography/tracks_10mio.tck IN/tractography/00_HDP/L_HDP.tck -maxlength 90 \
-tck_weights_in IN/tractography/sift2_weights_10mio.csv -tck_weights_out IN/tractography/00_HDP/L_HDP_weights.csv \
-include IN/atlasRegistration_lead/00_Distal/lh/STN.nii.gz -include IN/fs_parcellation/brodmann/L_BA4_BA6_dilate.mif \
-exclude IN/atlasRegistration_lead/00_Distal/lh/GPe_dilate.nii.gz -exclude IN/atlasRegistration_lead/00_Distal/lh/GPi.nii.gz \
-exclude IN/atlasRegistration_lead/00_Distal/lh/RN.nii.gz -exclude IN/atlasRegistration_lead/00_HMT/L_SN_sub.nii.gz \
-exclude IN/atlasRegistration_fsl/00_Striatum/striatum_coreg_dilate.nii.gz

for_each * : tckedit IN/tractography/tracks_10mio.tck IN/tractography/00_HDP/R_HDP.tck -maxlength 90 \
-tck_weights_in IN/tractography/sift2_weights_10mio.csv -tck_weights_out IN/tractography/00_HDP/R_HDP_weights.csv \
-include IN/atlasRegistration_lead/00_Distal/rh/STN.nii.gz -include IN/fs_parcellation/brodmann/R_BA4_BA6_dilate.mif \
-exclude IN/atlasRegistration_lead/00_Distal/rh/GPe_dilate.nii.gz -exclude IN/atlasRegistration_lead/00_Distal/rh/GPi.nii.gz \
-exclude IN/atlasRegistration_lead/00_Distal/rh/RN.nii.gz -exclude IN/atlasRegistration_lead/00_HMT/R_SN_sub.nii.gz \
-exclude IN/atlasRegistration_fsl/00_Striatum/striatum_coreg_dilate.nii.gz