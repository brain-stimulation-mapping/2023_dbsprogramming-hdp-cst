# For an individual subject, create dataframe **connectivity_fingerprint_weights.csv** containing:
# [ LeadID ContactID mu ..sum weights for each parcel ROI.. sum_w_VTA ]

from pathlib import Path
from collections import namedtuple
import numpy as np
import pandas as pd

subject = 'BaHe'
# Subject paths
mu_dir = '/home/alba/2021_HDP/03_Data/subjects/' + subject + '/tractography/mu.txt' #dir with mu file
dir = '/home/alba/2021_HDP/03_Data/subjects/' + subject + '/stimulations_all_simbio/MNI_ICBM_2009b_NLIN_ASYM' #dir with stim folders
dir_save = '/home/alba/2021_HDP/03_Data/subjects/' + subject + '/stimulations_all_simbio' #dir to save data

mu = np.genfromtxt(mu_dir) #subject's mu parameter
p = Path(dir) #subject's path

File = namedtuple('File', 'contact name mu path')  # Create file namedtuple
files = []  # Create empty list
df_w = pd.DataFrame() # create empty dataframe for weights

File_vat = namedtuple('File', 'name_w sum_w_VTA path_w')
files_vat = []  # Create empty list

# Iterate through path objects. Find files
for item in sorted(p.glob('**/*')):

    if item.name in ['connectivity_vat_L.csv'] or item.name in ['connectivity_vat_R.csv']:
        #print(item.name)
        name = item.name
        path = Path.resolve(item).parent
        contact = path.relative_to(dir)
        mu = mu
        weights = pd.read_csv(item,  header=None, delimiter=',', skiprows=1)
        df_w = df_w.append(weights, ignore_index=False)
        files.append(File(contact, name, mu, path))

    # Calculate VTA weights

    if item.name in ['tracks_vat_L_weights.csv'] or item.name in ['tracks_vat_R_weights.csv']:
        name_w = item.name
        path_w = Path.resolve(item).parent
        weights_vat = np.genfromtxt(item, delimiter=' ')
        sum_weights = np.sum(weights_vat)
        files_vat.append(File_vat(name_w, sum_weights, path_w))

# Create dataframes from list
df_vat = pd.DataFrame(files_vat)

df_data = pd.DataFrame(files)  # dataframe with data from .csv files --> contact name path
df_data['name'] = df_data['name'].astype('unicode')
df_data['contact'] = df_data['contact'].astype('unicode')
df_data['path'] = df_data['path'].astype('unicode')

# Add new column with lead L/R
df_data['lead'] = pd.DataFrame(df_data.name.str.split('_').str[2])  # split entry in substrings and keep the one in position 2
df_data['lead'] = df_data['lead'].replace('.csv', '', regex=True)

df_data['C'] = ""  # Create empty column

maskL = (df_data['lead'] == 'L')  # if 'L' in column lead
df_data['C'][maskL] = (df_data.contact.str.split('-').str[0])  # keep contactID in first position [0] --> left contact)

maskR = (df_data['lead'] == 'R')  # if 'R' in column lead
df_data['C'][maskR] = (df_data.contact.str.split('-').str[1])  # keep contactID in second position [1] --> right contact)

df_data['C'] = df_data['C'].replace('e', '', regex=True)  # Keep only numerical values in column C

# Add new columns with LeadID & ContactID
df_data['LeadID'] = pd.DataFrame((df_data.path.str.split('/').str[6]).map(str) + '_' + df_data.lead)
df_data['ContactID'] = pd.DataFrame((df_data.path.str.split('/').str[6]).map(str) + '_' + df_data.C)

# Reset index and rename columns weights
df_w = df_w.reset_index(drop=True)
df_w.columns = ['BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC', 'ACC', 'L_hippocampus', 'R_hippocampus', 'L_amygdala', 'R_amygdala']

# Concatenate dataframes
df = pd.concat([df_data['LeadID'], df_data['ContactID'], df_data['mu'], df_w, df_vat['sum_w_VTA']], axis=1)

# Save data
df.to_csv(dir_save + '/connectivity_fingerprint_weights.csv', index=False)