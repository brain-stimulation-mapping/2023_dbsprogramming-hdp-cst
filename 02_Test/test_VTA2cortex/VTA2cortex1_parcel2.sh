#!/bin/bash
# Connectivity fingerprint from VTA to freesurfer parcel

#Subject path
pat="DaHa"
subjectID="DaHa"
path="/home/alba/2021_HDP/03_Data/subjects"
LUT_path="/home/alba/03_Data/fs_LUT/"

#Tractography and weights files
tract=tracks_10mio.tck
weights=sift2_weights_10mio.csv

cd $path/$pat



### Obtain VTA to cortex connectivity fingerprint
for_each stimulations_all_simbio/MNI*/e* : tck2connectome IN/tracks_vat_L.tck $path/$pat/fs_parcellation/desikan/desikan_coreg.mif \
IN/connectivity_vat_desikan_L.csv -vector -tck_weights_in IN/tracks_vat_L_weights.csv

for_each stimulations_all_simbio/MNI*/e* : tck2connectome IN/tracks_vat_R.tck $path/$pat/fs_parcellation/desikan/desikan_coreg.mif \
IN/connectivity_vat_desikan_R.csv -vector -tck_weights_in IN/tracks_vat_R_weights.csv