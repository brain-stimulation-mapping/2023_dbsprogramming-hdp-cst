import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

#dir_save='C:/Users/Alba Segura Amil/OneDrive - Universitaet Bern/00_WORK/2021_HDP/FBCcalc_simbio_fastfield/'
#dir_save = '/home/alba/2021_HDP/03_Data/FBCcalc_simbio_fastfield/'
dir_save = '/mnt/data/2020_Projects/2021_HDP/03_Data/FBCcalc_simbio_fastfield/'

# load data
data = pd.read_csv(dir_save + 'connectivity_fingerprint_weights_2subjects.csv')
data['sum_w_parcel'] = data[['BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC', 'ACC', 'L_hippocampus',
                             'R_hippocampus', 'L_amygdala', 'R_amygdala']].sum(axis=1)

df_percent = data[['LeadID', 'ContactID','mu', 'BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC', 'ACC',
                   'L_hippocampus', 'R_hippocampus', 'L_amygdala', 'R_amygdala', 'sum_w_VTA', 'sum_w_parcel']]

df_mu = data[['LeadID', 'ContactID','mu', 'BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC', 'ACC',
                   'L_hippocampus', 'R_hippocampus', 'L_amygdala', 'R_amygdala', 'sum_w_VTA', 'sum_w_parcel']]


# function for percent
def percentage(number):
    return number * 100 / df_percent['sum_w_parcel']


def fbc(number):
    return number * df_percent['mu']


# executing the function
df_percent[['BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC', 'ACC', 'L_hippocampus', 'R_hippocampus',
            'L_amygdala', 'R_amygdala']] = df_percent[['BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC',
            'ACC', 'L_hippocampus', 'R_hippocampus', 'L_amygdala', 'R_amygdala']].apply(percentage)

df_mu[['BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC', 'ACC', 'L_hippocampus', 'R_hippocampus',
            'L_amygdala', 'R_amygdala']] = df_mu[['BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC',
            'ACC', 'L_hippocampus', 'R_hippocampus', 'L_amygdala', 'R_amygdala']].apply(fbc)


df_plot = df_percent[['BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC', 'ACC', 'L_hippocampus',
                      'R_hippocampus', 'L_amygdala', 'R_amygdala']]


df_plot.boxplot(grid=False, rot=60)
plt.ylabel('Weight percentage')
plt.title('VTA connectivity to cortex')
plt.show()


df_plot_mu = df_mu[['BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC', 'ACC', 'L_hippocampus',
                      'R_hippocampus', 'L_amygdala', 'R_amygdala']]

df_plot_mu.boxplot(grid=False, rot=60)
plt.ylabel('fbc')
plt.title('VTA connectivity to cortex')
plt.show()