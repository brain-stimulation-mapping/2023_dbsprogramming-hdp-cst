#!/bin/bash
# Obtain connectivity from VTA to cortex


#Subject path
pat="DaHa"
subjectID="DaHa"
path="/home/alba/2021_HDP/03_Data/subjects/DaHa/"
LUT_path="/home/alba/03_Data/fs_LUT/"

#Tractography and weights files
tract=tracks_10mio.tck
weights=sift2_weights_10mio.csv

cd $path/$pat

##### VTA's previously coregistered in 'VTAregister3'

### Obtain streamlines inside of each VTA            ##### stim at effect
#for_each stimulations_all_simbio/MNI*/e* : tckedit tractography/$tract IN/tracks_vat_L.tck \
#-include IN/vat_left_coreg.mif -tck_weights_in tractography/$weights -tck_weights_out IN/tracks_vat_L_weights.csv

#for_each stimulations_all_simbio/MNI*/e* : tckedit tractography/$tract IN/tracks_vat_R.tck \
#-include IN/vat_right_coreg.mif -tck_weights_in tractography/$weights -tck_weights_out IN/tracks_vat_R_weights.csv


### Extract cortical ROIs

#Extract BA
cd fs_parcellation/brodmann
mrcalc BA_exth_parcels_coreg.mif 1 -eq L_BA1.mif
mrcalc BA_exth_parcels_coreg.mif 2 -eq L_BA2.mif
mrcalc BA_exth_parcels_coreg.mif 3 -eq L_BA3a.mif
mrcalc BA_exth_parcels_coreg.mif 4 -eq L_BA3b.mif
mrcalc L_BA3a.mif L_BA3b.mif -max L_BA3.mif
mrcalc BA_exth_parcels_coreg.mif 5 -eq L_BA4a.mif
mrcalc BA_exth_parcels_coreg.mif 6 -eq L_BA4p.mif
mrcalc L_BA4a.mif L_BA4p.mif -max L_BA4.mif
mrcalc BA_exth_parcels_coreg.mif 7 -eq L_BA6.mif

mrcalc BA_exth_parcels_coreg.mif 15 -eq R_BA1.mif
mrcalc BA_exth_parcels_coreg.mif 16 -eq R_BA2.mif
mrcalc BA_exth_parcels_coreg.mif 17 -eq R_BA3a.mif
mrcalc BA_exth_parcels_coreg.mif 18 -eq R_BA3b.mif
mrcalc R_BA3a.mif R_BA3b.mif -max R_BA3.mif
mrcalc BA_exth_parcels_coreg.mif 19 -eq R_BA4a.mif
mrcalc BA_exth_parcels_coreg.mif 20 -eq R_BA4p.mif
mrcalc R_BA4a.mif R_BA4p.mif -max R_BA4.mif
mrcalc BA_exth_parcels_coreg.mif 21 -eq R_BA6.mif

#Sensory BA
mrcalc L_BA1.mif L_BA2.mif -max - | mrcalc - L_BA3.mif -max L_BA123.mif
mrcalc R_BA1.mif R_BA2.mif -max - | mrcalc - R_BA3.mif -max R_BA123.mif

#Motor BA
mrcalc L_BA4.mif L_BA6.mif -max L_BA46.mif
mrcalc R_BA4.mif R_BA6.mif -max R_BA46.mif

cd ../..

### Glasser atlas ROIs
mkdir fs_parcellation/glasser
# Map the annotations onto the volumetric image
mri_aparc2aseg --old-ribbon --s $subjectID --annot hcpmmp1 --o fs_parcellation/glasser/hcpmmp1.mgz
# Convert parcellations to Mrtrix format
labelconvert fs_parcellation/glasser/hcpmmp1.mgz $LUT_path/hcpmmp1_original.txt $LUT_path/hcpmmp1_ordered.txt fs_parcellation/glasser/hcpmmp1_parcels.mif
mrtransform fs_parcellation/glasser/hcpmmp1_parcels.mif -linear DWI/diff2struct_bbr_nearest_mrtrix.txt -inverse fs_parcellation/glasser/glasser_coreg.mif

cd fs_parcellation/glasser
# OFC
mrcalc glasser_coreg.mif 93 -eq L_ofc.mif
mrcalc glasser_coreg.mif 273 -eq R_ofc.mif
mrcalc glasser_coreg.mif 166 -eq L_pOFC.mif
mrcalc glasser_coreg.mif 346 -eq R_pOFC.mif
mrcalc L_ofc.mif L_pOFC.mif -max L_OFC.mif
mrcalc R_ofc.mif R_pOFC.mif -max R_OFC.mif

# vmPFC
mrcalc glasser_coreg.mif 65 -eq L_10r.mif;
mrcalc glasser_coreg.mif 245 -eq R_10r.mif;
mrcalc glasser_coreg.mif 88 -eq L_10v.mif;
mrcalc glasser_coreg.mif 268 -eq R_10v.mif;
mrcalc L_10r.mif L_10v.mif -max L_vmPFC.mif;
mrcalc R_10r.mif R_10v.mif -max R_vmPFC.mif;

# ACC
mrcalc glasser_coreg.mif 61 -eq L_a24.mif;
mrcalc glasser_coreg.mif 241 -eq R_a24.mif;
mrcalc glasser_coreg.mif 180 -eq L_p24.mif;
mrcalc glasser_coreg.mif 360 -eq R_p24.mif;
mrcalc L_a24.mif L_p24.mif -max L_ACC.mif;
mrcalc R_a24.mif R_p24.mif -max R_ACC.mif;

# DLPFC
mrcalc glasser_coreg.mif 73 -eq L_8C.mif;
mrcalc glasser_coreg.mif 67 -eq L_8Av.mif;
mrcalc glasser_coreg.mif 97 -eq L_i6-8.mif;
mrcalc glasser_coreg.mif 98 -eq L_s6-8.mif;
mrcalc glasser_coreg.mif 26 -eq L_SFL.mif;
mrcalc glasser_coreg.mif 70 -eq L_8BL.mif;
mrcalc glasser_coreg.mif 71 -eq L_9p.mif;
mrcalc glasser_coreg.mif 87 -eq L_9a.mif;
mrcalc glasser_coreg.mif 68 -eq L_8Ad.mif;
mrcalc glasser_coreg.mif 83 -eq L_p9-46v.mif;
mrcalc glasser_coreg.mif 85 -eq L_a9-46v.mif;
mrcalc glasser_coreg.mif 84 -eq L_46.mif;
mrcalc glasser_coreg.mif 86 -eq L_9-46d.mif;

mrcalc glasser_coreg.mif 253 -eq R_8C.mif;
mrcalc glasser_coreg.mif 247 -eq R_8Av.mif;
mrcalc glasser_coreg.mif 277 -eq R_i6-8.mif;
mrcalc glasser_coreg.mif 278 -eq R_s6-8.mif;
mrcalc glasser_coreg.mif 206 -eq R_SFL.mif;
mrcalc glasser_coreg.mif 250 -eq R_8BL.mif;
mrcalc glasser_coreg.mif 251 -eq R_9p.mif;
mrcalc glasser_coreg.mif 267 -eq R_9a.mif;
mrcalc glasser_coreg.mif 248 -eq R_8Ad.mif;
mrcalc glasser_coreg.mif 263 -eq R_p9-46v.mif;
mrcalc glasser_coreg.mif 265 -eq R_a9-46v.mif;
mrcalc glasser_coreg.mif 264 -eq R_46.mif;
mrcalc glasser_coreg.mif 266 -eq R_9-46d.mif;

mrcalc L_8C.mif L_8Av.mif -max - | mrcalc - L_i6-8.mif -max - | mrcalc - L_s6-8.mif -max - | mrcalc - L_SFL.mif -max -\| \
 mrcalc - L_8BL.mif -max - | mrcalc - L_9p.mif -max - | mrcalc - L_9a.mif -max - | mrcalc - L_8Ad.mif -max - | mrcalc - L_p9-46v.mif -max -\| \
 mrcalc - L_8BL.mif -max - | mrcalc - L_9p.mif -max - | mrcalc - L_9a.mif -max - | mrcalc - L_8Ad.mif -max - | mrcalc - L_p9-46v.mif -max -\| \

mrcalc R_a24.mif R_p24.mif -max R_ACC.mif;






