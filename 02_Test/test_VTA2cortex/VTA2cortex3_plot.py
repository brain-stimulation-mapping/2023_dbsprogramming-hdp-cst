# Create dataframe containg data for ALL subjects and visualize plots
# Save data in "FBCcalc_simbio_fastfield"


from pathlib import Path
import pandas as pd

# Create empty dataframes
data = pd.DataFrame(columns=['LeadID', 'ContactID', 'mu', 'BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC', 'ACC', 'L_hippocampus', 'R_hippocampus', 'L_amygdala', 'R_amygdala', 'sum_w_VTA'])

# loop in "subjects" folder to repeat process for each subject

rootdir = '/home/alba/2021_HDP/03_Data/subjects/'
entries = Path(rootdir)

#data = list()

for entry in sorted(entries.iterdir()): # each entry is one of the subjects' subfolder
    #print(entry.name)
    # LOAD DATA FROM .csv FILES
    for file in entry.glob('**/connectivity_fingerprint_weights.csv'):
        print(file.name)
        df = pd.read_csv(file)
        data = data.append(df[['LeadID', 'ContactID','mu', 'BA4', 'BA6', 'supFrontGy', 'midFrontGy', 'infFrontGy', 'OFC', 'ACC', 'L_hippocampus', 'R_hippocampus', 'L_amygdala', 'R_amygdala', 'sum_w_VTA']], ignore_index=True)

# save data
dir_save = '/home/alba/2021_HDP/03_Data/FBCcalc_simbio_fastfield/'
data.to_csv( dir_save + 'connectivity_fingerprint_weights_2subjects.csv', index=False)
