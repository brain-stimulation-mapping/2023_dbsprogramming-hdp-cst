#!/bin/bash
# Create parcellation image for VTA to cortex connectivity fingerprint

#Subject path
pat="BaHe"
subjectID="BaHe"
path="/home/alba/2021_HDP/03_Data/subjects"
LUT_path="/home/alba/03_Data/fs_LUT/"

#Tractography and weights files
tract=tracks_10mio.tck
weights=sift2_weights_10mio.csv

cd $path/$pat

### Cortial ROIs for parcellation
## Extract BA
cd fs_parcellation/brodmann
mrcalc BA_exth_parcels_coreg.mif 1 -eq L_BA1.mif
mrcalc BA_exth_parcels_coreg.mif 2 -eq L_BA2.mif
mrcalc BA_exth_parcels_coreg.mif 3 -eq L_BA3a.mif
mrcalc BA_exth_parcels_coreg.mif 4 -eq L_BA3b.mif
mrcalc L_BA3a.mif L_BA3b.mif -max L_BA3.mif
mrcalc BA_exth_parcels_coreg.mif 5 -eq L_BA4a.mif
mrcalc BA_exth_parcels_coreg.mif 6 -eq L_BA4p.mif
mrcalc L_BA4a.mif L_BA4p.mif -max L_BA4.mif
mrcalc BA_exth_parcels_coreg.mif 7 -eq L_BA6.mif

mrcalc BA_exth_parcels_coreg.mif 15 -eq R_BA1.mif
mrcalc BA_exth_parcels_coreg.mif 16 -eq R_BA2.mif
mrcalc BA_exth_parcels_coreg.mif 17 -eq R_BA3a.mif
mrcalc BA_exth_parcels_coreg.mif 18 -eq R_BA3b.mif
mrcalc R_BA3a.mif R_BA3b.mif -max R_BA3.mif
mrcalc BA_exth_parcels_coreg.mif 19 -eq R_BA4a.mif
mrcalc BA_exth_parcels_coreg.mif 20 -eq R_BA4p.mif
mrcalc R_BA4a.mif R_BA4p.mif -max R_BA4.mif
mrcalc BA_exth_parcels_coreg.mif 21 -eq R_BA6.mif

#Sensory BA
mrcalc L_BA1.mif L_BA2.mif -max - | mrcalc - L_BA3.mif -max L_BA123.mif
mrcalc R_BA1.mif R_BA2.mif -max - | mrcalc - R_BA3.mif -max R_BA123.mif

cd ../..

## ROIs from JHU labels atlas in MNI space
mkdir atlasRegistration_fsl/00_HarvardOxford
mrtransform /home/alba/03_Data/ATLAS/FSL_atlases/HarvardOxford/HarvardOxford-cort-maxprob-thr25-1mm.nii.gz \
-warp atlasRegistration_fsl/mrtrix_warp_corrected_inv.mif -interp nearest - | mrtransform - -linear DWI/diff2struct_bbr_nearest_mrtrix.txt \
-inverse atlasRegistration_fsl/00_HarvardOxford/HarvardOxford-cort-maxprob-thr25-1mm.nii.gz

cd atlasRegistration_fsl/00_HarvardOxford
# Associative areas: supFrontGy.mif   midFrontGy.mif  infFrontGy.mif
mrcalc HarvardOxford-cort-maxprob-thr25-1mm.nii.gz 3 -eq supFrontGy.mif
mrcalc HarvardOxford-cort-maxprob-thr25-1mm.nii.gz 4 -eq midFrontGy.mif
mrcalc HarvardOxford-cort-maxprob-thr25-1mm.nii.gz 5 -eq infFrontGy_tr.mif
mrcalc HarvardOxford-cort-maxprob-thr25-1mm.nii.gz 6 -eq infFrontGy_op.mif
mrcalc infFrontGy_tr.mif infFrontGy_op.mif -max infFrontGy.mif

# Limbic areas: OFC.mif   ACC.mif
mrcalc HarvardOxford-cort-maxprob-thr25-1mm.nii.gz 33 -eq OFC.mif
mrcalc HarvardOxford-cort-maxprob-thr25-1mm.nii.gz 29 -eq ACC_a.mif
mrcalc HarvardOxford-cort-maxprob-thr25-1mm.nii.gz 30 -eq ACC_p.mif
mrcalc ACC_a.mif ACC_p.mif -max ACC.mif

cd ../..

## ROIs from Juelich atlas in MNI space
mkdir atlasRegistration_fsl/00_Juelich
mrtransform /home/alba/03_Data/ATLAS/FSL_atlases/Juelich/Juelich-maxprob-thr25-1mm.nii.gz -warp atlasRegistration_fsl/mrtrix_warp_corrected_inv.mif -interp nearest - |\
 mrtransform - -linear DWI/diff2struct_bbr_nearest_mrtrix.txt  -inverse atlasRegistration_fsl/00_Juelich/Juelich-maxprob-thr25-1mm.nii.gz

cd atlasRegistration_fsl/00_Juelich
# Limbic areas: L/R_hippocampus.mif   L/R_amygdala.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 17 -eq Hip_cor_L.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 18 -eq Hip_cor_R.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 19 -eq Hip_ent_L.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 20 -eq Hip_ent_R.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 21 -eq Hip_den_L.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 22 -eq Hip_den_R.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 23 -eq Hip_tra_L.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 24 -eq Hip_tra_R.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 25 -eq Hip_sub_L.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 26 -eq Hip_sub_R.mif
mrcalc Hip_cor_L.mif  Hip_ent_L.mif -max - | mrcalc -  Hip_den_L.mif -max - | mrcalc - Hip_tra_L.mif -max - | mrcalc - Hip_sub_L.mif -max L_hippocampus.mif
mrcalc Hip_cor_R.mif  Hip_ent_R.mif -max - | mrcalc -  Hip_den_R.mif -max - | mrcalc - Hip_tra_R.mif -max - | mrcalc - Hip_sub_R.mif -max R_hippocampus.mif

# Limbic areas: L/R_amygdala.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 7 -eq Amyg_cen_L.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 8 -eq Amyg_cen_R.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 9 -eq Amyg_lat_L.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 10 -eq Amyg_lat_R.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 11 -eq Amyg_sup_L.mif
mrcalc Juelich-maxprob-thr25-1mm.nii.gz 12 -eq Amyg_sup_R.mif
mrcalc Amyg_cen_L.mif  Amyg_lat_L.mif -max - | mrcalc -  Amyg_sup_L.mif -max L_amygdala.mif
mrcalc Amyg_cen_R.mif  Amyg_lat_R.mif -max - | mrcalc -  Amyg_sup_R.mif -max R_amygdala.mif

cd ../..


### Create parcellation image
# Regriding BA images so they match dimensions of ROIs from FSL atlases
cd fs_parcellation/brodmann
mrcalc L_BA6.mif R_BA6.mif -max BA6.mif
mrgrid BA6.mif regrid -template ../../atlasRegistration_fsl/00_HarvardOxford/HarvardOxford-cort-maxprob-thr25-1mm.nii.gz BA6_regridFSLatlas.mif -interp nearest
mrcalc L_BA4.mif R_BA4.mif -max BA4.mif
mrgrid BA4.mif regrid -template ../../atlasRegistration_fsl/00_HarvardOxford/HarvardOxford-cort-maxprob-thr25-1mm.nii.gz BA4_regridFSLatlas.mif -interp nearest

cd ../..

# To avoid overlapping of ROIs, subtract sup/mid/infFrontGy from BA6.
cd atlasRegistration_fsl/00_HarvardOxford
mrcalc ../../fs_parcellation/brodmann/BA6_regridFSLatlas.mif supFrontGy.mif -max - | mrcalc - ../../fs_parcellation/brodmann/BA6_regridFSLatlas.mif -subtract supFrontGy_sub.mif;
mrcalc ../../fs_parcellation/brodmann/BA6_regridFSLatlas.mif midFrontGy.mif -max - | mrcalc - ../../fs_parcellation/brodmann/BA6_regridFSLatlas.mif -subtract midFrontGy_sub.mif;
mrcalc ../../fs_parcellation/brodmann/BA6_regridFSLatlas.mif infFrontGy.mif -max - | mrcalc - ../../fs_parcellation/brodmann/BA6_regridFSLatlas.mif -subtract infFrontGy_sub.mif

cd ../..

# Assign index values to ROIs
cd fs_parcellation/brodmann
mrcalc BA6_regridFSLatlas.mif 2 -mult BA6_regridFSLatlas.mif -force
cd ../../atlasRegistration_fsl/00_HarvardOxford
mrcalc supFrontGy_sub.mif 3 -mult  supFrontGy_sub.mif -force;
mrcalc midFrontGy_sub.mif 4 -mult  midFrontGy_sub.mif -force;
mrcalc infFrontGy_sub.mif 5 -mult  infFrontGy_sub.mif -force;
mrcalc OFC.mif 6 -mult  OFC.mif -force;
mrcalc ACC.mif 7 -mult  ACC.mif -force;
cd ../00_Juelich
mrcalc L_hippocampus.mif 8 -mult  L_hippocampus.mif -force;
mrcalc R_hippocampus.mif 9 -mult  R_hippocampus.mif -force;
mrcalc L_amygdala.mif 10 -mult  L_amygdala.mif -force;
mrcalc R_amygdala.mif 11 -mult  R_amygdala.mif -force

# Merge ROIs to create parcellation image
cd ../00_HarvardOxford
mrcalc ../../fs_parcellation/brodmann/BA4_regridFSLatlas.mif  ../../fs_parcellation/brodmann/BA6_regridFSLatlas.mif  -max - |\
 mrcalc - supFrontGy_sub.mif -max - | mrcalc - midFrontGy_sub.mif -max - | mrcalc -  infFrontGy_sub.mif -max - |\
 mrcalc -  OFC.mif -max - | mrcalc - ACC.mif -max - | mrcalc - ../00_Juelich/L_hippocampus.mif -max - |mrcalc - ../00_Juelich/R_hippocampus.mif -max - |\
 mrcalc - ../00_Juelich/L_amygdala.mif -max - | mrcalc - ../00_Juelich/R_amygdala.mif -max ../../STNconnectivity_parcel.mif
cd ../..

# Convert data to uint32 in order to use tckconnectome later
mrconvert STNconnectivity_parcel.mif STNconnectivity_parcel.mif -datatype uint32 -force


##### VTA's previously coregistered in 'VTAregister3'

### Obtain streamlines inside of each VTA            ##### stim at effect
#for_each stimulations_all_simbio/MNI*/e* : tckedit tractography/$tract IN/tracks_vat_L.tck \
#-include IN/vat_left_coreg.mif -tck_weights_in tractography/$weights -tck_weights_out IN/tracks_vat_L_weights.csv

#for_each stimulations_all_simbio/MNI*/e* : tckedit tractography/$tract IN/tracks_vat_R.tck \
#-include IN/vat_right_coreg.mif -tck_weights_in tractography/$weights -tck_weights_out IN/tracks_vat_R_weights.csv


### Obtain VTA to cortex connectivity fingerprint
for_each stimulations_all_simbio/MNI*/e* : tck2connectome IN/tracks_vat_L.tck $path/$pat/STNconnectivity_parcel.mif \
IN/connectivity_vat_L.csv -vector -tck_weights_in IN/tracks_vat_L_weights.csv

for_each stimulations_all_simbio/MNI*/e* : tck2connectome IN/tracks_vat_R.tck $path/$pat/STNconnectivity_parcel.mif \
IN/connectivity_vat_R.csv -vector -tck_weights_in IN/tracks_vat_R_weights.csv