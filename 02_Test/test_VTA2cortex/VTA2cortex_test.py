import pandas as pd
import matplotlib.pyplot as plt

dir_save = '/home/alba/2021_HDP/03_Data/subjects/DaHa/stimulations_all_simbio/MNI_ICBM_2009b_NLIN_ASYM/e1-9/'

df_L = pd.read_csv( dir_save + 'connectivity_vat_L.csv')


df_L.plot.bar()
plt.ylabel('Sum of weights')

plt.show()