# filter dataframes to keep only values where VTA_effect > VTA_noeffect

import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

dir_save = '/home/alba/2021_HDP/03_Data/FBCcalc_simbio_fastfield/'
#filename = 'data_effect_sift'  # name of the file to save
filename = 'data_side_effect_sift'

# Load data
# effect
#effect = pd.read_csv(dir_save + 'df_effect_sift.csv')
#no_effect = pd.read_csv(dir_save + 'df_no_effect_sift.csv')
# side effect
effect = pd.read_csv(dir_save + 'df_side_effect_sift.csv')
no_effect = pd.read_csv(dir_save + 'df_no_side_effect_sift.csv')

#### EFFECT
# Add new column with lead L/R
effect['lead'] = pd.DataFrame(effect.nameP.str.split('_').str[2])  # split entry in substrings and keep the one in position 2

# Create empty column
effect['C'] = ""

maskL = (effect['lead'] == 'L')  # if 'L' in column lead
effect['C'][maskL] = (effect.contactP.str.split('-').str[0])  # keep contactID in first position [0] --> left contact)

maskR = (effect['lead'] == 'R')  # if 'R' in column lead
effect['C'][maskR] = (effect.contactP.str.split('-').str[1])  # keep contactID in second position [1] --> right contact)

# Keep only numerical values in column C
effect['C'] = effect['C'].replace('s', '', regex=True)
effect['C'] = effect['C'].replace('e', '', regex=True)

# Add new columns with LeadID & ContactID
effect['LeadID'] = pd.DataFrame((effect.pathP.str.split('/').str[6]).map(str) + '_' + (effect.nameP.str.split('_').str[2]).map(str))
effect['ContactID'] = pd.DataFrame((effect.pathP.str.split('/').str[6]).map(str) + '_' + effect.C)


#### NO EFFECT
# Add new column with lead L/R
no_effect['lead'] = pd.DataFrame(no_effect.nameP.str.split('_').str[2])  # split entry in substrings and keep the one in position 2

#Create empty column
no_effect['C'] = ""

maskL = (no_effect['lead'] == 'L')  # if 'L' in column lead
no_effect['C'][maskL] = (no_effect.contactP.str.split('-').str[0])  # keep contactID in first position [0] --> left contact)

maskR = (no_effect['lead'] == 'R')  # if 'R' in column lead
no_effect['C'][maskR] = (no_effect.contactP.str.split('-').str[1])  # keep contactID in second position [1] --> right contact)

## keep only numerical values in column C
no_effect['C'] = no_effect['C'].replace('s', '', regex=True)
no_effect['C'] = no_effect['C'].replace('e', '', regex=True)

# Add new columns with LeadID & ContactID
no_effect['LeadID'] = pd.DataFrame((no_effect.pathP.str.split('/').str[6]).map(str) + '_' + (no_effect.nameP.str.split('_').str[2]).map(str))
no_effect['ContactID'] = pd.DataFrame((no_effect.pathP.str.split('/').str[6]).map(str) + '_' + no_effect.C)


########################
# Create reduced dataframes to compare columns
df_effect = effect[['LeadID', 'ContactID', 'percent']]
df_no_effect = no_effect[['LeadID', 'ContactID', 'percent']]

df_all = pd.concat([df_effect, df_no_effect], axis=1)
df_all['Compare lead'] = np.where(df_effect.LeadID == df_no_effect.LeadID, 'True', 'False')
df_all['Compare contact'] = np.where(df_effect.ContactID == df_no_effect.ContactID, 'True', 'False')
df_all['Compare percent'] = np.where(df_effect.percent > df_no_effect.percent, 'True', 'False')
df_effect_percent_false = df_all[df_all['Compare percent'] == 'False']
df_effect_percent_true = df_all[df_all['Compare percent'] == 'True']
#df_effect_lead_false = df_all[df_all['Compare lead'] == 'False']
#df_effect_contact_false = df_all[df_all['Compare contact'] == 'False']
###########################


# Select only columns of interest
data_effect = effect[['LeadID', 'ContactID', 'count', 'total_count', 'percent']]
data_effect['outcome'] = 1
data_no_effect = no_effect[['LeadID', 'ContactID', 'count', 'total_count', 'percent']]
data_no_effect['outcome'] = 0

# Concatenate and save dataframe
data = pd.concat([data_effect, data_no_effect], axis=0)
data.to_csv(dir_save + filename + '.csv', index=False)

# Keep only values where percent_effect > percent_no_effect  --> to not consider data where VTAs estimated with fastfield are larger than those estimated with simbio
df_percent_true_keys = df_effect_percent_true.iloc[:, [0,1]]  # df containing only values of 'LeadID' & 'ContactID' for percent TRUE
keys =list(df_percent_true_keys.columns.values)
i_percent_true = df_percent_true_keys.set_index(keys).index

i_effect = data_effect.set_index(keys).index
data_effect_true = data_effect[i_effect.isin(i_percent_true)] # df effect filtered TRUE

i_no_effect = data_no_effect.set_index(keys).index
data_no_effect_true = data_no_effect[i_no_effect.isin(i_percent_true)] # df no_effect filtered TRUE

data_true = pd.concat([data_effect_true, data_no_effect_true], axis=0)
data_true.to_csv(dir_save + filename + '_true.csv', index=False)