#!/bin/bash

path="/home/alba/2021_HDP/03_Data/subjects"
cd $path/IsNa

#### Tracking HDP
tckedit tractography/tracks_sift10M.tck tractography/L_STN_BA_90_sift10M.tck  -maxlength 90 \
-include atlasRegistration_lead/00_Distal/lh/STN.nii.gz -include fs_parcellation/brodmann/L_BA4_BA6.mif \
-exclude atlasRegistration_lead/00_Distal/lh/GPe.nii.gz -exclude atlasRegistration_lead/00_Distal/lh/GPi.nii.gz \
-exclude atlasRegistration_lead/00_Distal/lh/RN.nii.gz -exclude atlasRegistration_lead/00_HMT/L_SN_sub.nii.gz \
-exclude atlasRegistration_fsl/00_Striatum/striatum_coreg.nii.gz

tckedit tractography/tracks_sift10M.tck tractography/R_STN_BA_90_sift10M.tck -maxlength 90 \
-include atlasRegistration_lead/00_Distal/rh/STN.nii.gz -include fs_parcellation/brodmann/R_BA4_BA6.mif \
-exclude atlasRegistration_lead/00_Distal/rh/GPe.nii.gz -exclude atlasRegistration_lead/00_Distal/rh/GPi.nii.gz \
-exclude atlasRegistration_lead/00_Distal/rh/RN.nii.gz -exclude atlasRegistration_lead/00_HMT/R_SN_sub.nii.gz \
-exclude atlasRegistration_fsl/00_Striatum/striatum_coreg.nii.gz

#### Tracking CST
tckedit tractography/tracks_sift10M.tck tractography/00_CST/L_CST_sift10M.tck \
-include fs_parcellation/brodmann/L_BA4_BA6.mif \
-include atlasRegistration_fsl/00_JHU/L_supCorRad.mif -include atlasRegistration_fsl/00_JHU/L_postLimbIC.mif \
-include atlasRegistration_fsl/00_JHU/L_ped.mif  -include fs_parcellation/brainstem/medulla.mif \
-exclude atlasRegistration_fsl/00_JHU/CC.mif -exclude fs_parcellation/desikan/L_cerebellum.mif \
-exclude fs_parcellation/desikan/R_cerebellum.mif -minlength 80 -maxlength 135

tckedit tractography/tracks_sift10M.tck tractography/00_CST/R_CST_sift10M.tck \
-include fs_parcellation/brodmann/R_BA4_BA6.mif \
-include atlasRegistration_fsl/00_JHU/R_supCorRad.mif -include atlasRegistration_fsl/00_JHU/R_postLimbIC.mif \
-include atlasRegistration_fsl/00_JHU/R_ped.mif  -include fs_parcellation/brainstem/medulla.mif \
-exclude atlasRegistration_fsl/00_JHU/CC.mif -exclude fs_parcellation/desikan/L_cerebellum.mif \
-exclude fs_parcellation/desikan/R_cerebellum.mif -minlength 80 -maxlength 135