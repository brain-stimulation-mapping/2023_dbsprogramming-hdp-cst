#!/bin/bash
#Run SIFT and extract HDP and CST for group of subjects

path="/home/alba/2021_HDP/03_Data/subjects"
cd $path

#### SIFT on 10M tractogram --> reduce to 2M


for_each * : tcksift -act IN/DWI/5tt_hsvs_coreg.mif IN/tractography/tracks_10mio.tck IN/DWI/wmfod_group_norm.mif \
IN/tractography/tracks_sift2M.tck -out_mu IN/tractography/mu_sift2M.txt

#### Tracking HDP
for_each * : tckedit IN/tractography/tracks_sift2M.tck IN/tractography/L_STN_BA_90_sift2M.tck  -maxlength 90 \
-include IN/atlasRegistration_lead/00_Distal/lh/STN.nii.gz -include IN/fs_parcellation/brodmann/L_BA4_BA6.mif \
-exclude IN/atlasRegistration_lead/00_Distal/lh/GPe.nii.gz -exclude IN/atlasRegistration_lead/00_Distal/lh/GPi.nii.gz \
-exclude IN/atlasRegistration_lead/00_Distal/lh/RN.nii.gz -exclude IN/atlasRegistration_lead/00_HMT/L_SN_sub.nii.gz \
-exclude IN/atlasRegistration_fsl/00_Striatum/striatum_coreg.nii.gz

for_each * : tckedit IN/tractography/tracks_sift2M.tck IN/tractography/R_STN_BA_90_sift2M.tck -maxlength 90 \
-include IN/atlasRegistration_lead/00_Distal/rh/STN.nii.gz -include IN/fs_parcellation/brodmann/R_BA4_BA6.mif \
-exclude IN/atlasRegistration_lead/00_Distal/rh/GPe.nii.gz -exclude IN/atlasRegistration_lead/00_Distal/rh/GPi.nii.gz \
-exclude IN/atlasRegistration_lead/00_Distal/rh/RN.nii.gz -exclude IN/atlasRegistration_lead/00_HMT/R_SN_sub.nii.gz \
-exclude IN/atlasRegistration_fsl/00_Striatum/striatum_coreg.nii.gz

#### Tracking CST
for_each * : tckedit IN/tractography/tracks_sift2M.tck IN/tractography/00_CST/L_CST_sift2M.tck \
-include IN/fs_parcellation/brodmann/L_BA4_BA6.mif \
-include IN/atlasRegistration_fsl/00_JHU/L_supCorRad.mif -include IN/atlasRegistration_fsl/00_JHU/L_postLimbIC.mif \
-include IN/atlasRegistration_fsl/00_JHU/L_ped.mif  -include IN/fs_parcellation/brainstem/medulla.mif \
-exclude IN/atlasRegistration_fsl/00_JHU/CC.mif -exclude IN/fs_parcellation/desikan/L_cerebellum.mif \
-exclude IN/fs_parcellation/desikan/R_cerebellum.mif -minlength 80 -maxlength 135

for_each * : tckedit IN/tractography/tracks_sift2M.tck IN/tractography/00_CST/R_CST_sift2M.tck \
-include IN/fs_parcellation/brodmann/R_BA4_BA6.mif \
-include IN/atlasRegistration_fsl/00_JHU/R_supCorRad.mif -include IN/atlasRegistration_fsl/00_JHU/R_postLimbIC.mif \
-include IN/atlasRegistration_fsl/00_JHU/R_ped.mif  -include IN/fs_parcellation/brainstem/medulla.mif \
-exclude IN/atlasRegistration_fsl/00_JHU/CC.mif -exclude IN/fs_parcellation/desikan/L_cerebellum.mif \
-exclude IN/fs_parcellation/desikan/R_cerebellum.mif -minlength 80 -maxlength 135


#### Obtain streamlines inside of each VTA     ##### stim at effect
#Effect
for_each */stimulations_all_simbio/MNI*/e* : tckedit */tractography/$tract1 IN/tracks_vat_$tract1 -include IN/vat_left_coreg.mif
for_each */stimulations_all_simbio/MNI*/e* : tckedit */tractography/$tract2 IN/tracks_vat_$tract2 -include IN/vat_right_coreg.mif
# Side Effect
for_each */stimulations_all_simbio/MNI*/se* : tckedit */tractography/00_CST/$tract3 IN/tracks_vat_$tract3 -include IN/vat_left_coreg.mif
for_each */stimulations_all_simbio/MNI*/se* : tckedit */tractography/00_CST/$tract4 IN/tracks_vat_$tract4 -include IN/vat_right_coreg.mif

#### Obtain streamlines inside of each VTA    ##### stim at NO effect
#Effect
for_each */stimulations_noeffect/* : tckedit */tractography/$tract1 IN/tracks_vat_$tract1 -include IN/vat_left_coreg.mif
for_each */stimulations_noeffect/* : tckedit */tractography/$tract2 IN/tracks_vat_$tract2 -include IN/vat_right_coreg.mif
# Side Effect
for_each */stimulations_noeffect/* : tckedit */tractography/00_CST/$tract3 IN/tracks_vat_$tract3 -include IN/vat_left_coreg.mif
for_each */stimulations_noeffect/* : tckedit */tractography/00_CST/$tract4 IN/tracks_vat_$tract4 -include IN/vat_right_coreg.mif
