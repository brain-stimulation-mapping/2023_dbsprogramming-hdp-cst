# For each subject, create dataframes for effect and side_effect containing the following data:
# ['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']
# Export dataframes --> effect.csv  &  sideeffect.csv

from pathlib import Path
from collections import namedtuple
import numpy as np
import pandas as pd
import re

subject = 'BaHe'
# Subject paths

dir = '/home/alba/2021_HDP/03_Data/subjects/' + subject + '/stimulations_all_simbio/MNI_ICBM_2009b_NLIN_ASYM' #dir with stim folders
dir_save = '/home/alba/2021_HDP/03_Data/subjects/' + subject + '/stimulations_all_simbio' #dir to save data


p = Path(dir) #subject's path

#Create file namedtuple
FileP = namedtuple('FileP', 'contactP nameP count total_count percent pathP')
#Create empty list
filesP = []

#Iterate through path objects. Find all files with .csv extension
for item in sorted(p.glob('**/*')):

    if item.suffix in ['.tck']:
        nameP = item.name
        pathP = Path.resolve(item).parent
        contactP = pathP.relative_to(dir)
        # Open the and read .tck file (header + binary file --> 'rb')
        file = open(item, 'rb')
        content = file.readlines()
        # Get values of count and tot_count
        stream_count = re.findall('count: \d+', str(content[:]))  # retrieve values for count and total_count
        count = re.findall('\d+', str(stream_count[0]))  # retrieve numerical value for count
        count = int(count[0])
        total_count = re.findall('\d+', str(stream_count[1]))  # retrieve numerical value for total_count
        total_count = int(total_count[0])
        # Calculate activation percentage
        percent = count * 100 / total_count
        filesP.append(FileP(contactP, nameP, count, total_count, percent, pathP))

# Create dataframe from list
df_all= pd.DataFrame(filesP)  # dataframe with data from .tck files --> streamlines
df_all['contactP'] = df_all['contactP'].astype('unicode')  # convert to unicode to be able to use .str
df_all['nameP'] = df_all['nameP'].astype('unicode')
df_sift = df_all[df_all['nameP'].str.contains('sift2M')]  #keep only rows with sift

effect = df_sift[~df_sift['contactP'].str.contains('s')]  # effect at effect
sideeffect = df_sift[df_sift['contactP'].str.contains('s')]  # side effect at effect

# Save data
effect.to_csv(dir_save + '/effect_sift.csv', index=False)
sideeffect.to_csv(dir_save + '/side_effect_sift.csv', index=False)

