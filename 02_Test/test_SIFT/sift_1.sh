#!/bin/bash

path="/home/alba/2021_HDP/03_Data/subjects"
cd $path/DaHa

tckgen -act DWI/5tt_hsvs_coreg.mif -backtrack -crop_at_gmwmi -seed_dynamic DWI/wmfod_group_norm.mif \
-select 100M DWI/wmfod_group_norm.mif tractography/tracks_100mio.tck

tcksift -act DWI/5tt_hsvs_coreg.mif tractography/tracks_100mio.tck DWI/wmfod_group_norm.mif \
tractography/tracks_sift10M.tck -out_mu tractography/mu_sift10M.txt

#tcksift:        Iteration     Removed     Remaining     Cost fn
#tcksift: [done]  123899            0       1112337       2.92%


cd ../IsNa

tckgen -act DWI/5tt_hsvs_coreg.mif -backtrack -crop_at_gmwmi -seed_dynamic DWI/wmfod_group_norm.mif \
-select 100M DWI/wmfod_group_norm.mif tractography/tracks_100mio.tck
tcksift -act DWI/5tt_hsvs_coreg.mif tractography/tracks_100mio.tck DWI/wmfod_group_norm.mif \
tractography/tracks_sift10M.tck -out_mu tractography/mu_sift10M.txt

#tcksift:        Iteration     Removed     Remaining     Cost fn
#tcksift: [done]  139104            0       1639625       3.86%
