# Calculate percent_sum_w and plot different variables

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

dir_save = '/mnt/data/2020_Projects/2021_HDP/03_Data/FBCcalc_simbio_fastfield_20subj/'

# load data
HDP = pd.read_csv(dir_save + 'data_effect0.csv')
CST= pd.read_csv(dir_save + 'data_side_effect0.csv')

# load total_sum_w  ###dataframes created in 'total_sum_w.py'
sum_w_HDP = pd.read_csv(dir_save + 'df_total_sum_w_HDP.csv')
sum_w_CST = pd.read_csv(dir_save + 'df_total_sum_w_CST.csv')

# extract colums as lists and create dictionaries
LeadID_HDP = sum_w_HDP['LeadID'].to_list()
total_sum_w_HDP = sum_w_HDP['total_sum_w'].to_list()
dict_sum_w_HDP = dict(zip(LeadID_HDP, total_sum_w_HDP))

LeadID_CST = sum_w_CST['LeadID'].to_list()
total_sum_w_CST = sum_w_CST['total_sum_w'].to_list()
dict_sum_w_CST = dict(zip(LeadID_CST, total_sum_w_CST))

#use dictionaries for creating new column in E and SE
HDP.insert(6, 'total_sum_w', HDP['LeadID'].map(dict_sum_w_HDP))
CST.insert(6, 'total_sum_w', CST['LeadID'].map(dict_sum_w_CST))

#Add percent_w colums
HDP.insert(7, 'percent_w', HDP['sum_w']*100/HDP['total_sum_w'])
CST.insert(7, 'percent_w', CST['sum_w']*100/CST['total_sum_w'])


#### Plot HDP & CST curves together
HDP['pathway'] = 'HDP'
CST['pathway'] = 'CST'
df_pathways = pd.concat([HDP, CST], axis=0)
#df_pathways.to_csv(dir_save + 'df_pathways.csv', index=False)

#plt.rcParams['font.size'] = '16'
plt.subplots()
#sns.set_theme()
#sns.set_theme(style="whitegrid")
#sns.set_style("whitegrid")
sns.lmplot(x="percent", y="outcome", hue="pathway", data=df_pathways, logistic=True, y_jitter=.03)
plt.xscale('log')
plt.axhline(y=0.5, color='grey', linestyle='--')
plt.show()

sns.lmplot(x="sum_w", y="outcome", hue="pathway", data=df_pathways, logistic=True, y_jitter=.03)
plt.xscale('log')
plt.axhline(y=0.5, color='grey', linestyle='--')
plt.show()

sns.lmplot(x="percent_w", y="outcome", hue="pathway", data=df_pathways, logistic=True, y_jitter=.03)
plt.xscale('log')
plt.axhline(y=0.5, color='grey', linestyle='--')
plt.xlabel("% Streamlines's weights")
plt.ylabel('Outcome')
plt.show()

sns.lmplot(x="fbc", y="outcome", hue="pathway", data=df_pathways, logistic=True, y_jitter=.03)
plt.xscale('log')
plt.axhline(y=0.5, color='grey', linestyle='--')
plt.show()
#plt.title('')





fig, ax = plt.subplots()
sns.lmplot(x="percent", y="outcome", hue="pathway", data=df_pathways, logistic=True, y_jitter=.03)
plt.xscale('log')
#plt.axhline(y=0.5, color='grey', linestyle='--')
ax2 = ax.twinx()
sns.lmplot(x="sum_w", y="outcome", hue="pathway", data=df_pathways, logistic=True, y_jitter=.03)
plt.xscale('log')
plt.show()
