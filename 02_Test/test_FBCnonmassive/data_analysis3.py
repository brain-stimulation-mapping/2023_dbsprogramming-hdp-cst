# Modify dataframes to set to 0 values where VTA_noeffect > VTA_effect
# saves data as **data_effect0** & **data_side_effect0**

import pandas as pd
import numpy as np

#dir_save='C:/Users/Alba Segura Amil/OneDrive - Universitaet Bern/00_WORK/2021_HDP/FBCcalc_simbio_fastfield/'
dir_save = '/mnt/data/2020_Projects/2021_HDP/03_Data/FBCcalc_simbio_fastfield_20subj/'

filename = 'data_side_effect0_CST_3'  # name of the file to save

# Load data
# Effect
#effect = pd.read_csv(dir_save + 'df_effect.csv')
#no_effect = pd.read_csv(dir_save + 'df_no_effect.csv')

# Side effect
#effect = pd.read_csv(dir_save + 'df_side_effect.csv')
#no_effect = pd.read_csv(dir_save + 'df_no_side_effect.csv')
effect = pd.read_csv(dir_save + 'data_CST_3.csv')
no_effect = pd.read_csv(dir_save + 'data_CST_ne_3.csv')

#### EFFECT
# Add new column with lead L/R
effect['lead'] = pd.DataFrame(effect.name.str.split('_').str[3])  # split entry in substrings and keep the one in position 2

# Create empty column
effect['C'] = ""

maskL = (effect['lead'] == 'L')  # if 'L' in column lead
effect['C'][maskL] = (effect.contact.str.split('-').str[0])  # keep contactID in first position [0] --> left contact)

maskR = (effect['lead'] == 'R')  # if 'R' in column lead
effect['C'][maskR] = (effect.contact.str.split('-').str[1])  # keep contactID in second position [1] --> right contact)

# Keep only numerical values in column C
effect['C'] = effect['C'].replace('s', '', regex=True)
effect['C'] = effect['C'].replace('e', '', regex=True)

# Add new columns with LeadID & ContactID
effect['LeadID'] = pd.DataFrame((effect.path.str.split('/').str[7]).map(str) + '_' + (effect.name.str.split('_').str[3]).map(str))
effect['ContactID'] = pd.DataFrame((effect.path.str.split('/').str[7]).map(str) + '_' + (effect.C))

#### NO EFFECT
# Add new column with lead L/R
no_effect['lead'] = pd.DataFrame(no_effect.name.str.split('_').str[3])  # split entry in substrings and keep the one in position 2

# Create empty column
no_effect['C'] = ""
maskL = (no_effect['lead'] == 'L')  # if 'L' in column lead
no_effect['C'][maskL] = (no_effect.contact.str.split('-').str[0])  # keep contactID in first position [0] --> left contact)
maskR = (no_effect['lead'] == 'R')  # if 'R' in column lead
no_effect['C'][maskR] = (no_effect.contact.str.split('-').str[1])  # keep contactID in second position [1] --> right contact)

# keep only numerical values in column C
no_effect['C'] = no_effect['C'].replace('s', '', regex=True)
no_effect['C'] = no_effect['C'].replace('e', '', regex=True)

# Add new columns with LeadID & ContactID
no_effect['LeadID'] = pd.DataFrame((no_effect.path.str.split('/').str[7]).map(str) + '_' + (no_effect.name.str.split('_').str[3]).map(str))
no_effect['ContactID'] = pd.DataFrame((no_effect.path.str.split('/').str[7]).map(str) + '_' + (no_effect.C))

# Create reduced dataframes to compare columns
df_effect = effect[['LeadID', 'ContactID', 'count', 'total_count', 'percent', 'sum_w', 'fbc']]
df_no_effect = no_effect[['LeadID', 'ContactID', 'count', 'total_count', 'percent', 'sum_w', 'fbc']]

# Add compare_percent column in df_no_effect
df_no_effect['Compare_percent'] = np.where(df_effect.percent > df_no_effect.percent, 'True', 'False')

# convert true/false to 0/1
df_no_effect['Compare_percent_int'] = (df_no_effect['Compare_percent'] == 'True').astype(int)

# Duplicate columns percent & fbc
df_no_effect['percent0'] = df_no_effect['percent']
df_no_effect['sum_w0'] = df_no_effect['sum_w']
df_no_effect['fbc0'] = df_no_effect['fbc']

# Replace percent & fbc false values for 0
df_no_effect.loc[df_no_effect['Compare_percent_int'] == 0, 'percent0'] = 0
df_no_effect.loc[df_no_effect['Compare_percent_int'] == 0, 'sum_w0'] = 0
df_no_effect.loc[df_no_effect['Compare_percent_int'] == 0, 'fbc0'] = 0

# Keep columns of interest
df_no_effect0 = df_no_effect[['LeadID', 'ContactID', 'count', 'total_count', 'percent0', 'sum_w0', 'fbc0']]
#Rename columns for concatenation
df_no_effect0.rename(columns={'percent0': 'percent', 'sum_w0': 'sum_w', 'fbc0': 'fbc'}, inplace=True)

#Add outcome columns
df_effect['outcome'] = 1
df_no_effect0['outcome'] = 0

# Concatenate and save dataframe
data = pd.concat([df_effect, df_no_effect0], axis=0)
data.to_csv(dir_save + filename + '.csv', index=False)



# Save additional dataframes for plotting top responders
#df_effect.to_csv(dir_save + 'df_side_effect_mA.csv', index=False)
#df_no_effect0.to_csv(dir_save + 'df_no_effect_mA.csv', index=False)

