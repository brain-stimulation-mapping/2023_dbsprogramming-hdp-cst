# Create and save dataframes containg data for ALL subjects: effect/no_effect --- side_effect/no_side_effect
# Save data in "FBCcalc_simbio_fastfield"
# df_effect.csv, df_no_effect.csv, df_side_effect.csv, df_no_side_effect.csv

from pathlib import Path
import pandas as pd

# Create empty dataframes
df_fbc_ef = pd.DataFrame(columns=['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path'])
df_fbc_Nef = pd.DataFrame(columns=['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path'])
df_fbc_sef = pd.DataFrame(columns=['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path'])
df_fbc_Nsef = pd.DataFrame(columns=['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path'])

# loop in "subjects" folder to repeat process for each subject
rootdir = '/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/'
entries = Path(rootdir)

for entry in sorted(entries.iterdir()):  # each entry is one of the subjects' subfolder
    print(entry.name)
    # LOAD DATA FROM .csv FILES
    for file in entry.glob('**/effect.csv'):
        df_effect = pd.read_csv(file)  # dataframe effect

    for file in entry.glob('**/ne_effect.csv'):
        df_ne_effect = pd.read_csv(file)  # dataframe ne_effect
        df_ne_effect = df_ne_effect.replace('ne', 'e', regex=True) # replace name of contacts so they match strings in df_effect

    for file in entry.glob('**/df_e_CST_3.csv'):
    #for file in entry.glob('**/side_effect.csv'):
        df_side_effect = pd.read_csv(file)  # df side_effect

    for file in entry.glob('**/df_ne_CST_3.csv'):
    #for file in entry.glob('**/ne_side_effect.csv'):
        df_ne_side_effect = pd.read_csv(file)  # df ne_side_effect
        df_ne_side_effect = df_ne_side_effect.replace('ne', 'se', regex=True)  # replace name of contacts so they match strings in df_side_effect

    # EFFECT
    # Obtain FCB values of ne_effect contacts which are present in effect
    df_effect_cn = df_effect[['contact', 'name']] # df containing only values of 'contact' & 'name'
    keys = list(df_effect_cn.columns.values)
    i_effect = df_effect_cn.set_index(keys).index
    i_ne_effect = df_ne_effect.set_index(keys).index
    df_ne_effect_filtered = df_ne_effect[i_ne_effect.isin(i_effect)] # df ne_effect with filtered contacts

    # SIDE EFFECT
    # Obtain FCB values of ne_side_effect contacts which are present in side_effect
    df_side_effect_cn = df_side_effect[['contact', 'name']]  # df containing only values of 'contact' & 'name'
    keys = list(df_side_effect_cn.columns.values)
    i_side_effect = df_side_effect_cn.set_index(keys).index
    i_ne_side_effect = df_ne_side_effect.set_index(keys).index
    df_ne_side_effect_filtered = df_ne_side_effect[i_ne_side_effect.isin(i_side_effect)]  # df ne_effect with filtered contacts

    # Append obtained fbc values to dataframes
    df_fbc_ef = df_fbc_ef.append(df_effect[['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']], ignore_index=True)
    df_fbc_Nef = df_fbc_Nef.append(df_ne_effect_filtered[['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']], ignore_index=True)
    df_fbc_sef = df_fbc_sef.append(df_side_effect[['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']], ignore_index=True)
    df_fbc_Nsef = df_fbc_Nsef.append(df_ne_side_effect_filtered[['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'fbc', 'path']], ignore_index=True)

# Save data
dir_save = '/mnt/data/2020_Projects/2021_HDP/03_Data/FBCcalc_simbio_fastfield_20subj/'
#df_fbc_ef.to_csv(dir_save + 'df_effect.csv', index=False)
#df_fbc_Nef.to_csv(dir_save + 'df_no_effect.csv', index=False)
#df_fbc_sef.to_csv(dir_save + 'df_side_effect.csv', index=False)
#df_fbc_Nsef.to_csv(dir_save + 'df_no_side_effect.csv', index=False)
df_fbc_sef.to_csv(dir_save + 'data_CST_3.csv', index=False)
df_fbc_Nsef.to_csv(dir_save + 'data_CST_ne_3.csv', index=False)
