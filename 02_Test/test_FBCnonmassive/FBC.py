# For each subject, create dataframes for effect and side_effect containing the following data:
# ['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'total_w', 'percent_w', 'fbc', 'path']
# Export dataframes --> effect.csv  &  ne_effect.csv  &&  side_effect.csv & ne_side_effect.csv

# run twice per subject, once for effect and once for no effect
# modify dir, dir_save and name to save

from pathlib import Path
from collections import namedtuple
import numpy as np
import pandas as pd
import re

subject = 'MaMa'
# Subject paths
mu_dir = '/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/' + subject + '/tractography/mu.txt' #dir with mu file

filename_HDP = 'effect'  # name of the file to save
filename_CST = 'df_e_CST_3'  # name of the file to save

## EFFECT
dir = '/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/' + subject + '/stimulations_all_simbio/MNI_ICBM_2009b_NLIN_ASYM' #dir with stim folders
dir_save = '/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/' + subject + '/stimulations_all_simbio/' #dir to save data

## NO EFFECT
#dir = '/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/' + subject + '/stimulations_noeffect' #dir with stim folders
#dir_save = '/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/' + subject + '/stimulations_noeffect/' #dir to save data

## PREDS
#dir = '/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/' + subject + '/stimulations_preds' #dir with stim folders
#dir_save = '/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/' + subject + '/stimulations_preds/' #dir to save data

mu = np.genfromtxt(mu_dir) #subject's mu parameter
p = Path(dir) #subject's path

#Create files namedtuple
File = namedtuple('File', 'contact name sum_w fbc path')
FileS = namedtuple('FileS', 'contactS nameS count total_count percent pathS')

#Create empty lists
files = []
filesS = []

#Iterate through path objects. Find all files with name
for item in sorted(p.glob('**/*')):

    if item.name in ['tracks_vat_L_STN_BA_90_weights.csv'] or item.name in ['tracks_vat_R_STN_BA_90_weights.csv'] \
            or item.name in ['tracks_vat_3_L_CST_weights.csv'] or item.name in ['tracks_vat_3_R_CST_weights.csv']:

        name = item.name
        path = Path.resolve(item).parent
        contact = path.relative_to(dir)
        weights = np.genfromtxt(item, delimiter=' ')
        sum_w = np.sum(weights)
        fbc = mu * sum_w
        files.append(File(contact, name, sum_w, fbc, path))

    if item.name in ['tracks_vat_L_STN_BA_90.tck'] or item.name in ['tracks_vat_R_STN_BA_90.tck']\
            or item.name in ['tracks_vat_3_L_CST.tck'] or item.name in ['tracks_vat_3_R_CST.tck']:

        nameS = item.name
        pathS = Path.resolve(item).parent
        contactS = pathS.relative_to(dir)
        # Open the and read .tck file (header + binary file --> 'rb')
        file = open(item, 'rb')
        content = file.readlines()
        # Get values of count and tot_count
        stream_count = re.findall('count: \d+', str(content[:]))  # retrieve values for count and total_count
        count = re.findall('\d+', str(stream_count[0]))  # retrieve numerical value for count
        count = int(count[0])
        total_count = re.findall('\d+', str(stream_count[1]))  # retrieve numerical value for total_count
        total_count = int(total_count[0])
        # Calculate activation percentage
        percent = count * 100 / total_count
        filesS.append(FileS(contactS, nameS, count, total_count, percent, pathS))


# Create dataframes from lists
df1 = pd.DataFrame(files)  # dataframe with data from .csv files --> weights
df2 = pd.DataFrame(filesS)  # dataframe with data from .tck files --> streamlines
df = pd.concat([df1['contact'], df1['name'], df2['count'], df2['total_count'], df2['percent'],
                    df1['sum_w'], df1['fbc'], df1['path']], axis=1)  # concatenate both dataframes

# Export dataframe to .csv file
df_HDP = df[df['name'].str.contains('STN')]
df_CST = df[df['name'].str.contains('CST')]

#df_HDP.to_csv(dir_save + filename_HDP + '.csv', index=False)
df_CST.to_csv(dir_save + filename_CST + '.csv', index=False)