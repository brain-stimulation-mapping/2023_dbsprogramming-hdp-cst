% patientFolder, stimLabel, side, cathode, stimAmplitude, anode, stimType, estimateInTemplate)

patientFolder = '/mnt/data/2020_Projects/2021_HDP/03_Data/LeadDBS/Pat_ToDo_preds';
%contacts = ["e1-9_"; "e2-10_"; "e3-11_"; "e4-12_"; "e5-13_"; "e6-14_"; "e7-15_"; "e8-16_"; "e234-101112_"; "e567-131415_"]; %contacts labels 
contacts = ["e1-9_"]; %contacts labels 
side = 'right';
cathode_ID = [0 1 2 3 4 5 6 7]'; % ea_gevat_wrapper adds an offeset --> same values for rh & lh
%cathode_indices = [ 1 1; 2 2; 3 3; 4 4; 5 5; 6 6; 7 7;...
 %   8 8; 2 4; 5 7];
%cathode_indices = [2 4; 5 7];
cathode_indices = [1 1];
anode = -1;
stimType = 'current';
estimateInTemplate = true;
stimAmplitude = 0.5:.5:1;

failedVTAs = {}; % To store a list of the failed VTAs

for i = 1:length(contacts)
     
    try
        stimLabelBase = contacts{i};
        cathode = cathode_ID( cathode_indices(i, 1):cathode_indices(i, 2) );

        for indexAmplitude = 1:numel( stimAmplitude )
            ea_genvat_wrapper( ...
                patientFolder, ...
                [ stimLabelBase num2str( stimAmplitude( indexAmplitude )) ],...
                side,...
                cathode,...
                stimAmplitude( indexAmplitude ),...
                anode, ...
                stimType, ...
                estimateInTemplate );
        end

        catch exception
            failedVTAs = [failedVTAs;[ stimLabelBase num2str( stimAmplitude( indexAmplitude )) ] ];
        continue;  % Jump to next iteration of: for i
    end    
end
