% patientFolder, stimLabel, side, cathode, stimAmplitude, anode, stimType, estimateInTemplate)
patientFolder = '/mnt/data/2020_Projects/2021_HDP/03_Data/LeadDBS/Pat_WiDa_preds';
stimLabelBase = 'e8-16_';
side = 'left';
cathode = 7; % ea_gevat_wrapper adds an offeset --> same values for rh & lh
anode = -1;
stimType = 'current';
estimateInTemplate = true;

stimAmplitude = 0.5:0.5:5;

failedVTAs = {}; % To store a list of the failed VTAs

for indexAmplitude = 1:numel( stimAmplitude )
    
  try
    
     ea_genvat_wrapper( ...
        patientFolder, ...
        [ stimLabelBase num2str( stimAmplitude( indexAmplitude )) ],...
         side,...
         cathode,...
         stimAmplitude( indexAmplitude ),...
         anode, ...
         stimType, ...
         estimateInTemplate );
    
    catch exception
        failedVTAs = [failedVTAs;[ stimLabelBase num2str( stimAmplitude( indexAmplitude )) ] ];
        
    continue;  % Jump to next iteration of: for i
    
  end
       
end