#!/bin/bash
# Alba Segura Amil, october '22

# Co-register and calculate streamlines inside of VTAs for a group of subjects (massive VTA calculation)

# Requirements before running script:
# 1. Khoa creates VTAs for 20 subjects 1mA .. 0.5 .. 8mA
#   (data in /mnt/data/2020_Projects/2021_HDP/03_Data/PatientVTAs/03_Data/Pat_*)

# 2. HDP & CTS streamlines previously obtained
#     (data in /mnt/data/2020_Projects/2021_HDP/03_Data/subjects/*/tractography/00_CST_5/
#              /mnt/data/2020_Projects/2021_HDP/03_Data/subjects/*/tractography/ )

# 3. Transforms between MNI space and diffusion space (non-linear and linear) previously obtained
#    (data in /mnt/data/2020_Projects/2021_HDP/03_Data/subjects/*/atlasRegistration_lead/diff2struct_bbr_mrtrix.txt
#             /mnt/data/2020_Projects/2021_HDP/03_Data/subjects/*/atlasRegistration_lead/mrtrix_warp_corrected_inv.mif )

## ----------------------------------------
## Inputs:
# - path to subjects folder: $path
# - path to LeadDBS subjects folder containing all VTAs: $subj_VTA
# - Left HDP tracks and weights: $tract1 & $weight1
# - Right HDP tracks and weights: $tract2 & $weight2
# - Left CST tracks and weights: $tract3 & $weight3
# - Right CST tracks and weights: $tract4 & $weight4
# - Transform files in atlasRegistration_lead: mrtrix_warp_corrected_inv.mif & diff2struct_bbr_mrtrix.txt

## Outputs:
# - vat_left/right.nii from MNI to diffusion space
# - streamlines inside of each VTA: tracks_vat_L_STN_BA_90.tck & tracks_vat_L_STN_BA_90_weights.csv
#                                   tracks_vat_L_CST.tck & tracks_vat_L_CST_weights.csv

##------------------------------------------
path="/mnt/data/2020_Projects/2021_HDP/03_Data/subjects" # path to subjects folder
subj_VTA="/mnt/data/2020_Projects/2021_HDP/03_Data/PatientVTAs/03_Data"  # path to LeadDBS subjects folder containing all VTAs

CST=00_CST_5 # folder with CST tracts

tract1=L_STN_BA_90.tck
weight1=L_STN_BA_90_weights.csv

tract2=R_STN_BA_90.tck
weight2=R_STN_BA_90_weights.csv

tract3=L_CST.tck
weight3=L_CST_weights.csv

tract4=R_CST.tck
weight4=R_CST_weights.csv

cd $subj_VTA

for dir in * ; do
    echo "$dir"
    dir="$dir"

    #### Coregister vat_left/right.nii from MNI to diffusion space
    for_each $dir/stimulations/MNI_ICBM_2009b_NLIN_ASYM/* : mrtransform -warp $path/$dir/atlasRegistration_lead/mrtrix_warp_corrected_inv.mif IN/vat_left.nii -\| \
      mrtransform - -linear $path/$dir/atlasRegistration_lead/diff2struct_bbr_mrtrix.txt -inverse IN/vat_left_coreg.nii.gz

    for_each $dir/stimulations/MNI_ICBM_2009b_NLIN_ASYM/* : mrtransform -warp $path/$dir/atlasRegistration_lead/mrtrix_warp_corrected_inv.mif IN/vat_right.nii -\| \
      mrtransform - -linear $path/$dir/atlasRegistration_lead/diff2struct_bbr_mrtrix.txt -inverse IN/vat_right_coreg.nii.gz

    #### Obtain streamlines inside of each VTA
    # Effect
    for_each $dir/stimulations/MNI_ICBM_2009b_NLIN_ASYM/* : tckedit $path/$dir/tractography/$tract1 IN/tracks_vat_$tract1 \
     -include IN/vat_left_coreg.nii.gz -tck_weights_in $path/$dir/tractography/$weight1 -tck_weights_out IN/tracks_vat_$weight1 -force

    for_each $dir/stimulations/MNI_ICBM_2009b_NLIN_ASYM/* : tckedit $path/$dir/tractography/$tract2 IN/tracks_vat_$tract2 \
     -include IN/vat_right_coreg.nii.gz -tck_weights_in $path/$dir/tractography/$weight2 -tck_weights_out IN/tracks_vat_$weight2 -force

    # Side Effect
    for_each $dir/stimulations/MNI_ICBM_2009b_NLIN_ASYM/* : tckedit $path/$dir/tractography/$CST/$tract3 IN/tracks_vat_$tract3 \
     -include IN/vat_left_coreg.mif -tck_weights_in $path/$dir/tractography/$CST/$weight3 -tck_weights_out IN/tracks_vat_$weight3 -force

    for_each $dir/stimulations/MNI_ICBM_2009b_NLIN_ASYM/* : tckedit $path/$dir/tractography/$CST/$tract4 IN/tracks_vat_$tract4 \
      -include IN/vat_right_coreg.nii.gz -tck_weights_in $path/$dir/tractography/$CST/$weight4 -tck_weights_out IN/tracks_vat_$weight4 -force
done