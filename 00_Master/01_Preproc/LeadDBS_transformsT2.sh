#!/bin/bash
# Alba Segura Amil, october '22

# TODO check correct execution, script was used for another project

# Lead-DBS transforms
# Convert LeadDBS ants transform (MNI-LeadDBS to anat_T1) for usage in MRtrix.
# Obtain transform anat_t1 to diffusion


## ----------------------------------------
# Inputs:
# - path to subjects folder: $path
# - subject ID in subjects folder: $pat
# - path to LeadDBS folder containing subject's reconstruction: $path_leaddbs
# - Patient T1 in LeadDBS: $T1_pat
# - MNI-LeadDBS T1: $T1_MNI

## Outputs in /atlasRegistration_lead:
# - mrtrix_warp_corrected_inv.mif
# - diff2struct_lead_bbr.mat & diff2struct_bbr_mrtrix.txt
# - anat_t2_coreg.mif

##------------------------------------------
path="/mnt/data/2020_Projects/2021_HDP/03_Data/subjects" # path to subjects folder
pat = '' # patID in subjects folder
path_leaddbs="/mnt/data/2020_Projects/2021_HDP/03_Data/LeadDBS/Pat_$pat" # path to LeadDBS folder containing subject's reconstruction
T2_pat="anat_t2.nii" # Patient T2 in LeadDBS
T2_MNI="/mnt/data/04_CodeBase/leaddbs251/templates/space/MNI_ICBM_2009b_NLIN_ASYM/t2.nii"  # LeadDBS-MNI T2

cd $path/$pat
#mkdir atlasRegistration_lead # this folder is already created if LeadDBS_transforms.sh is run first
cd atlasRegistration_lead

# Warp MNI_leadDBS to anat_t1
#mrconvert $path_leaddbs/$T2_pat T2_leaddbs.mif
#warpinit T2_leaddbs.mif identity_warp[].nii
for i in {0..2};
do antsApplyTransforms -d 3 -e 0 -i identity_warp${i}.nii -o mrtrix_warp${i}.nii -r $T2_MNI -t $path_leaddbs/glanatComposite.nii.gz --default-value 2147483647;
done
warpcorrect mrtrix_warp[].nii mrtrix_warp_corrected.mif -marker 2147483647
mrtransform $path_leaddbs/$T2_pat -warp mrtrix_warp_corrected.mif T2inMNIlead.mif
### Check result of co-registration
warpinvert mrtrix_warp_corrected.mif mrtrix_warp_corrected_inv.mif

# Transform anat_t2 to diffusion
bet $path_leaddbs/$T2_pat anat_t2_bet -m -f 0.4
#fast -t 2 anat_t2_bet

#flirt -in $path/$pat/DWI/b0_preproc_bet -ref anat_t2_bet -dof 6 -omat diff2struct_lead_initial.mat
#flirt -in $path/$pat/DWI/b0_preproc_bet -ref anat_t2_bet -dof 6 -cost bbr -wmseg anat_t2_bet_pve_2 -init diff2struct_lead_initial.mat \
 #-omat diff2struct_lead_bbr.mat -schedule /usr/local/fsl/etc/flirtsch/bbr.sch
#transformconvert diff2struct_lead_bbr.mat $path/$pat/DWI/b0_preproc_bet.nii.gz anat_t2_bet.nii.gz flirt_import diff2struct_bbr_mrtrix.txt -force
#mrtransform $path_leaddbs/$T2_pat -linear diff2struct_bbr_mrtrix.txt -inverse anat_t2_coreg.mif -force


flirt -in $path/$pat/DWI/b0_preproc_bet -ref anat_t2_bet -dof 12 -omat diff2struct_lead.mat
transformconvert diff2struct_lead.mat $path/$pat/DWI/b0_preproc_bet.nii.gz anat_t2_bet.nii.gz flirt_import diff2struct_bbr_mrtrix.txt -force
mrtransform $path_leaddbs/$T2_pat -linear diff2struct_bbr_mrtrix.txt -inverse anat_t2_coreg.mif -force