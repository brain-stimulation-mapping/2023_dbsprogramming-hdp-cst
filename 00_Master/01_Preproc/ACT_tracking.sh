#!/bin/bash
# Alba Segura Amil, october '22

# Script for anatomically-constrained tractography (ACT), whole-brain tractography and SIFT2.
# If 5ttgen hsvs, requires to run recon-all first (fs_parcel.sh (1)).
# Also requires diff2struct transforms (diff2struct.sh (4)).

## ----------------------------------------
# Inputs:
# - path to subjects folder: $path
# - patID in subjects folder: $pat
# - freesurfer subject ID: $subjectID

## Outputs:
# - 5tt_hsvs.mif & 5tt_hsvs_coreg.mif
# - tracks_10mio.tck
# - sift2_weights_10mio.csv & mu.txt


#TODO
# if subjects ID would be the same in the freesurfer/recon-all folder and in the data/subjects folder,
# $pat & $subjectID could be the same variable

##------------------------------------------
path="/home/alba/2021_HDP/03_Data/subjects" # path to subjects folder
pat='' # patID in subjects folder
subjectID='' # freesurfer subject ID

cd $path/$pat/DWI

# ACT hsvs
# 5ttgen hsvs requires to previously run freesurfer
5ttgen hsvs $SUBJECTS_DIR/$subjectID 5tt_hsvs.mif

# To corregister the 5tt output to diffusion, previously obtain transform diff2struct_bbr_mrtrix.txt (run diff2struct script)
mrtransform 5tt_hsvs.mif -linear diff2struct_bbr_mrtrix.txt -inverse 5tt_hsvs_coreg.mif
# If bbr transform not available, run line below instead
#mrtransform 5tt_hsvs.mif -linear diff2struct_initial_mrtrix.txt -inverse 5tt_hsvs_coreg.mif

# Generate WB tractogram --> 10M streamlines
mkdir ../tractography
tckgen -act 5tt_hsvs_coreg.mif -backtrack -crop_at_gmwmi -seed_dynamic wmfod_norm.mif -select 10M wmfod_norm.mif ../tractography/tracks_10mio.tck;
tcksift2 -act 5tt_hsvs_coreg.mif ../tractography/tracks_10mio.tck wmfod_norm.mif ../tractography/sift2_weights_10mio.csv -out_mu ../tractography/mu.txt


# If freesurfer output not available, run ACT fsl instead
#5ttgen fsl ../T1/t1_unb.nii.gz 5tt_fsl.mif

# To corregister the 5tt output to diffusion, previously obtain transform diff2struct_bbr_mrtrix.txt (run diff2struct script)
#mrtransform 5tt_fsl.mif -linear diff2struct_bbr_mrtrix.txt -inverse 5tt_fsl_coreg.mif

# WB tractogram --> 10M streamlines
#mkdir ../tractography
#tckgen -act 5tt_fsl_coreg.mif -backtrack -crop_at_gmwmi -seed_dynamic wmfod_norm.mif -select 10M wmfod_norm.mif ../tractography/tracks_10mio.tck
#tcksift2 -act 5tt_fsl_coreg.mif ../tractography/tracks_10mio.tck wmfod_norm.mif ../tractography/sift2_weights_10mio.csv -out_mu ../tractography/mu.txt