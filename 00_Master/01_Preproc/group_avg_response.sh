 #!/bin/bash
# Alba Segura Amil, october '22

# Script for group analysis (for inter-subject comparison)
# Response function, group average response function, DWI masks, fiber orientation distribution, intensity normalization

## ----------------------------------------
# Inputs:
# - path to subjects folder: $path
# -  00_eddy/ output

## Outputs:
# - dwi_preproc.mif
# - response_wm.txt response_gm.txt response_csf.txt
# - group_average_response_wm.txt group_average_response_gm.txt group_average_response_csf.txt
# - mask_preproc.mif
# - wmfod_group.mif gmfod_group.mif csffod_group.mif
# - wmfod_group_norm.mif gmfod_group_norm.mif csffod_group_norm.mif

##------------------------------------------
# path to subjects folder
#path="/home/alba/2021_HDP/03_Data/subjects"
path='/mnt/data/2020_Projects/2026_FMPD/subjects'

cd $path

#Response function estimation
for_each * : dwi2response dhollander IN/DWI/dwi_preproc.mif IN/DWI/response_wm.txt IN/DWI/response_gm.txt IN/DWI/response_csf.txt

#Group average response function
responsemean */DWI/response_wm.txt ../group_average_response_wm.txt
responsemean */DWI/response_gm.txt ../group_average_response_gm.txt
responsemean */DWI/response_csf.txt ../group_average_response_csf.txt

# Mask estimation
for_each * : dwi2mask IN/DWI/dwi_preproc.mif IN/DWI/mask_preproc.mif

############ Multi-shell data
# Fiber orientation distribution
for_each * : dwi2fod msmt_csd IN/DWI/dwi_preproc.mif ../group_average_response_wm.txt IN/DWI/wmfod_group.mif \
../group_average_response_gm.txt IN/DWI/gmfod_group.mif  ../group_average_response_csf.txt IN/DWI/csffod_group.mif -mask IN/DWI/mask_preproc.mif

# Intensity normalisation
for_each * : mtnormalise IN/DWI/wmfod_group.mif IN/DWI/wmfod_group_norm.mif IN/DWI/gmfod_group.mif IN/DWI/gmfod_group_norm.mif \
IN/DWI/csffod_group.mif IN/DWI/csffod_group_norm.mif -mask IN/DWI/mask_preproc.mif


########### Single shell data
#for_each * : dwi2fod msmt_csd IN/DWI/dwi_preproc.mif -mask IN/DWI/mask_preproc.mif \
 #../group_average_response_wm.txt IN/DWI/wmfod_group.mif ../group_average_response_csf.txt IN/DWI/csffod_group.mif;
#for_each * : mtnormalise IN/DWI/wmfod_group.mif IN/DWI/wmfod_group_norm.mif IN/DWI/csffod_group.mif IN/DWI/csffod_group_norm.mif \
 #-mask IN/DWI/mask_preproc.mif