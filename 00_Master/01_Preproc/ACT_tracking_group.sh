#!/bin/bash
# Alba Segura Amil, october '22

# Script for anatomically-constrained tractography (ACT), whole-brain tractography and SIFT2.
# Use with group avereage response functions.
# If 5ttgen hsvs, requires to run recon-all first (fs_parcel.sh (1)).
# Also requires diff2struct transforms (diff2struct.sh (4)).

## ----------------------------------------
# Inputs:
# - path to subjects folder: $path
# - patID in subjects folder: $pat
# - freesurfer subject ID: $subjectID

## Outputs:
# - 5tt_hsvs.mif & 5tt_hsvs_coreg.mif
# - tracks_10mio.tck
# - sift2_weights_10mio.csv & mu.txt

##------------------------------------------
#path="/home/alba/2021_HDP/03_Data/subjects" # path to subjects folder
path='/mnt/data/2020_Projects/2026_FMPD/subjects'
fs_dir='/mnt/data/2020_Projects/2026_FMPD/fs_recon' #if fs_recon not in default folder

cd $path

########## ACT
## Run for each subject individually --> 5ttgen hsvs $SUBJECTS_DIR/$subject  $subject /DWI/5tt_hsvs.mif
for_each * : 5ttgen hsvs $SUBJECTS_DIR/IN  IN/DWI/5tt_hsvs.mif #if fs_subject == subject --> TODO check if works ### if freesurfer subjects dir is the default one
for_each * : mrtransform IN/DWI/5tt_hsvs.mif -linear IN/DWI/diff2struct_bbr_mrtrix.txt -inverse IN/DWI/5tt_hsvs_coreg.mif


########## Tracking
#for_each * : mkdir IN/tractography
# Tracking 10M streamlines
for_each * : tckgen -act IN/DWI/5tt_hsvs_coreg.mif -backtrack -crop_at_gmwmi -seed_dynamic IN/DWI/wmfod_group_norm.mif \
-select 10M IN/DWI/wmfod_group_norm.mif IN/tractography/tracks_10mio.tck
# SIFT2
for_each * : tcksift2 -act IN/DWI/5tt_hsvs_coreg.mif IN/tractography/tracks_10mio.tck IN/DWI/wmfod_group_norm.mif \
IN/tractography/sift2_weights_10mio.csv -out_mu IN/tractography/mu.txt