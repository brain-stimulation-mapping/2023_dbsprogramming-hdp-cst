#!/bin/bash
# Alba Segura Amil, october '22

# Lead-DBS transforms
# Convert LeadDBS ants transform (MNI-LeadDBS to anat_T1) for usage in MRtrix.
# Obtain transform anat_t1 to diffusion


## ----------------------------------------
# Inputs:
# - path to subjects folder: $path
# - subject ID in subjects folder: $pat
# - path to LeadDBS folder containing subject's reconstruction: $path_leaddbs
# - Patient T1 in LeadDBS: $T1_pat
# - MNI-LeadDBS T1: $T1_MNI

## Outputs in /atlasRegistration_lead:
# - mrtrix_warp_corrected_inv.mif
# - diff2struct_lead_bbr.mat & diff2struct_bbr_mrtrix.txt
# - anat_t1_coreg.mif

##------------------------------------------
path="/mnt/data/2020_Projects/2021_HDP/03_Data/subjects" # path to subjects folder
pat = '' # patID in subjects folder
path_leaddbs="/mnt/data/2020_Projects/2021_HDP/03_Data/LeadDBS/Pat_$pat" # path to LeadDBS folder containing subject's reconstruction
T1_pat="anat_t1.nii" # Patient T1 in LeadDBS
T1_MNI="/mnt/data/04_CodeBase/leaddbs251/templates/space/MNI_ICBM_2009b_NLIN_ASYM/t1.nii" # LeadDBS-MNI T1

cd $path/$pat
mkdir atlasRegistration_lead # folder to save transformations
cd atlasRegistration_lead

# Warp MNI-LeadDBS to anat_t1
mrconvert $path_leaddbs/$T1_pat T1_leaddbs.mif
warpinit T1_leaddbs.mif identity_warp[].nii
for i in {0..2};
do antsApplyTransforms -d 3 -e 0 -i identity_warp${i}.nii -o mrtrix_warp${i}.nii -r $T1_MNI -t $path_leaddbs/glanatComposite.h5 --default-value 2147483647;
done
warpcorrect mrtrix_warp[].nii mrtrix_warp_corrected.mif -marker 2147483647
mrtransform $path_leaddbs/$T1_pat -warp mrtrix_warp_corrected.mif T1inMNIlead.mif
### Check result of co-registration ants
warpinvert mrtrix_warp_corrected.mif mrtrix_warp_corrected_inv.mif

# Transform anat_t1 to diffusion
bet $path_leaddbs/$T1_pat anat_t1_bet -m -f 0.2
fast anat_t1_bet

#for subjects2, change transform's name diff2struct_fsl* for diff2struct_lead* --> to avoid missunderstandings (IGNORE FOR NEW SUBJECTS)
flirt -in $path/$pat/DWI/b0_preproc_bet -ref anat_t1_bet -dof 6 -omat diff2struct_lead_initial.mat
flirt -in $path/$pat/DWI/b0_preproc_bet -ref anat_t1_bet -dof 6 -cost bbr -wmseg anat_t1_bet_pve_2 -init diff2struct_lead_initial.mat \
 -omat diff2struct_lead_bbr.mat -schedule /usr/local/fsl/etc/flirtsch/bbr.sch
transformconvert diff2struct_lead_bbr.mat $path/$pat/DWI/b0_preproc_bet.nii.gz anat_t1_bet.nii.gz flirt_import diff2struct_bbr_mrtrix.txt
mrtransform $path_leaddbs/$T1_pat -linear diff2struct_bbr_mrtrix.txt -inverse anat_t1_coreg.mif


#flirt -in b0_preproc_bet -ref anat_t1_bet -dof 7 -omat diff2struct_fsl_initial.mat
#flirt -in b0_preproc_bet -ref anat_t1_bet -dof 7 -cost bbr -wmseg anat_t1_bet_pve_2 -init diff2struct_fsl_initial.mat \
#-omat diff2struct_fsl_bbr.mat -schedule /usr/local/fsl/etc/flirtsch/bbr.sch
#transformconvert diff2struct_fsl_bbr.mat b0_preproc_bet.nii.gz anat_t1_bet.nii.gz flirt_import diff2struct_bbr_mrtrix.txt -force
#mrtransform $path_leaddbs/$T1_pat -linear diff2struct_bbr_mrtrix.txt -inverse anat_t1_coreg.mif -force