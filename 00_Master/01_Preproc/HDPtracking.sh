#!/bin/bash
# Alba Segura Amil, october '22

# Obtain Hyperdirect pathway (HDP) from whole-brain tractogram

## ----------------------------------------
## Inputs:
# - path to subjects folder: $path
# - subject ID in subjects folder: $pat
# - folder containig atlases defined in MNI space: $ATLAS
# - cortical areas from freesurfer Brodmann parcellation: L_BA4_BA6.mif & R_BA4_BA6.mif

## Outputs:
# - Distal atlas structures in diffusion space (e.g., STN.nii.gz)
# - Substantia nigra in diffusion space (substracted from the STN): L_SN_sub.nii.gz & R_SN_sub.nii.gz
# - Striatum in diffusion space: striatum_coreg.nii.gz
# - L_STN_BA_90.tck & R_STN_BA_90.tck
# - L_STN_BA_90_weights.csv & R_STN_BA_90_weights.csv

##------------------------------------------
path="/mnt/data/2020_Projects/2021_HDP/03_Data/subjects" # path to subjects folder
pat='' # patID in subjects folder
ATLAS="/home/alba/03_Data/ATLAS" # folder containing atlases defined in MNI space

cd $path/$pat

## Coregister Distal atlas (defined in MNI-LeadDBs space)
mkdir atlasRegistration_lead/00_Distal
mkdir atlasRegistration_lead/00_Distal/lh
mkdir atlasRegistration_lead/00_Distal/rh
for_each $ATLAS/DISTAL_Minimal_Ewert2017/lh/* : mrtransform IN -warp atlasRegistration_lead/mrtrix_warp_corrected_inv.mif -interp nearest - \| mrtransform\
 - -linear atlasRegistration_lead/diff2struct_bbr_mrtrix.txt -inverse atlasRegistration_lead/00_Distal/lh/NAME
for_each $ATLAS/DISTAL_Minimal_Ewert2017/rh/* : mrtransform IN -warp atlasRegistration_lead/mrtrix_warp_corrected_inv.mif -interp nearest - \| mrtransform\
 - -linear atlasRegistration_lead/diff2struct_bbr_mrtrix.txt -inverse atlasRegistration_lead/00_Distal/rh/NAME

## Coregister Substantia Nigra (Human Motor Thalamus atlas defined in MNI-LeadDBS)
mkdir atlasRegistration_lead/00_HMT
mrtransform $ATLAS/HumanMotorThalamus_Ilinsky2017/lh/SN.nii.gz -warp atlasRegistration_lead/mrtrix_warp_corrected_inv.mif -interp nearest - | mrtransform \
- -linear atlasRegistration_lead/diff2struct_bbr_mrtrix.txt -inverse atlasRegistration_lead/00_HMT/L_SN.nii.gz
mrtransform $ATLAS/HumanMotorThalamus_Ilinsky2017/rh/SN.nii.gz -warp atlasRegistration_lead/mrtrix_warp_corrected_inv.mif -interp nearest - | mrtransform \
 - -linear atlasRegistration_lead/diff2struct_bbr_mrtrix.txt -inverse atlasRegistration_lead/00_HMT/R_SN.nii.gz

## Subtract STN from Substantia Nigra (to be safe in case there is overlap between the structures)
cd atlasRegistration_lead
mrcalc 00_HMT/L_SN.nii.gz 00_Distal/lh/STN.nii.gz -max - | mrcalc - 00_Distal/lh/STN.nii.gz -subtract 00_HMT/L_SN_sub.nii.gz
mrcalc 00_HMT/R_SN.nii.gz 00_Distal/rh/STN.nii.gz -max - | mrcalc - 00_Distal/rh/STN.nii.gz -subtract 00_HMT/R_SN_sub.nii.gz
cd ..

## Coregister Striatum (Striatum atlas in MNI-FSL --> parcellation atlas (i.e., all structures are defined in the same file with different index values))
mkdir atlasRegistration_fsl/00_Striatum
mrtransform $ATLAS/FSL_atlases/Striatum/striatum-structural-1mm.nii.gz -warp atlasRegistration_fsl/mrtrix_warp_corrected_inv.mif -interp nearest - | mrtransform \
- -linear DWI/diff2struct_bbr_nearest_mrtrix.txt -inverse atlasRegistration_fsl/00_Striatum/striatum_coreg.nii.gz

## HDP using exclusion regions
cd tractography
# Max length 90
tckedit tracks_10mio.tck L_STN_BA_90.tck -include ../atlasRegistration_lead/00_Distal/lh/STN.nii.gz -include ../fs_parcellation/brodmann/L_BA4_BA6.mif \
-exclude ../atlasRegistration_lead/00_Distal/lh/GPe.nii.gz -exclude ../atlasRegistration_lead/00_Distal/lh/GPi.nii.gz \
-exclude ../atlasRegistration_lead/00_Distal/lh/RN.nii.gz -exclude ../atlasRegistration_lead/00_HMT/L_SN_sub.nii.gz \
-exclude ../atlasRegistration_fsl/00_Striatum/striatum_coreg.nii.gz \
-tck_weights_in sift2_weights_10mio.csv -tck_weights_out L_STN_BA_90_weights.csv -maxlength 90;

tckedit tracks_10mio.tck R_STN_BA_90.tck -include ../atlasRegistration_lead/00_Distal/rh/STN.nii.gz -include ../fs_parcellation/brodmann/R_BA4_BA6.mif \
-exclude ../atlasRegistration_lead/00_Distal/rh/GPe.nii.gz -exclude ../atlasRegistration_lead/00_Distal/rh/GPi.nii.gz \
-exclude ../atlasRegistration_lead/00_Distal/rh/RN.nii.gz -exclude ../atlasRegistration_lead/00_HMT/R_SN_sub.nii.gz \
-exclude ../atlasRegistration_fsl/00_Striatum/striatum_coreg.nii.gz \
-tck_weights_in sift2_weights_10mio.csv -tck_weights_out R_STN_BA_90_weights.csv -maxlength 90

# Max length 100
#tckedit tracks_10mio.tck L_STN_BA_100.tck -include ../atlasRegistration/00_Distal/lh/STN.nii.gz -include ../fs_parcellation/brodmann/L_BA4_BA6.mif \
#-exclude ../atlasRegistration/00_Distal/lh/GPe.nii.gz -exclude ../atlasRegistration/00_Distal/lh/GPi.nii.gz -exclude ../atlasRegistration/00_Distal/lh/RN.nii.gz -exclude ../atlasRegistration/00_Striatum/striatum_coreg.nii.gz -exclude ../atlasRegistration/00_HMT/L_SN_sub.nii.gz \
#-tck_weights_in sift2_weights_10mio.csv -tck_weights_out L_STN_BA_100_weights.csv -maxlength 100

#tckedit tracks_10mio.tck R_STN_BA_100.tck -include ../atlasRegistration/00_Distal/rh/STN.nii.gz -include ../fs_parcellation/brodmann/R_BA4_BA6.mif \
#-exclude ../atlasRegistration/00_Distal/rh/GPe.nii.gz -exclude ../atlasRegistration/00_Distal/rh/GPi.nii.gz -exclude ../atlasRegistration/00_Distal/rh/RN.nii.gz -exclude ../atlasRegistration/00_Striatum/striatum_coreg.nii.gz -exclude ../atlasRegistration/00_HMT/R_SN_sub.nii.gz \
#-tck_weights_in sift2_weights_10mio.csv -tck_weights_out R_STN_BA_100_weights.csv -maxlength 100