#!/bin/bash
# Alba Segura Amil, october '22

# Script to coregister b0 to T1 & T2 using flirt and bbr

# Note !!!
# For the HDP paper analysis I used bet for the brain extraction, but synthstrip-docker seems to be more robust

## ----------------------------------------
# Inputs:
# - path to subjects folder: $path
# - subject ID in subjects folder: $pat

## Outputs:
# - b0_preproc.nii.gz & b0_preproc_bet.nii.gz
# - t1_unb.nii.gz & t1_unb_bet.nii.gz
# - t2_bet.nii.gz
# - diff2struct_fsl_bbr.mat (fsl transform file) & diff2struct_bbr_mrtrix.txt (mrtrix transform file)
# - diff2struct_fsl_bbr_nearest.mat & diff2struct_bbr_nearest_mrtrix.txt
# - t1_coreg.mif
# - t2_bet.nii.gz flirt_import & diff2structT2_mrtrix.txt
# - t2_coreg.mif

##------------------------------------------
# path to subjects folder
path="/home/alba/2021_HDP/03_Data/subjects"
# patID in subjects folder
pat = ''

# Coregister b0 to T1 using flirt and bbr
cd $path/$pat
cd DWI
# If only one b0 in dwi_preproc
dwiextract -bzero dwi_preproc.mif b0_preproc.nii.gz
# If multiple b0 in dwi_preproc
#dwiextract -bzero dwi_preproc.mif  - | mrmath - mean b0_preproc.nii.gz

#bet b0_preproc b0_preproc_bet -m -f 0.2
python3 /mnt/data/04_CodeBase/synthstrip-docker -i b0_preproc.nii.gz -o b0_preproc_bet.nii.gz -m b0_preproc_bet_mask.nii.gz

cd ../T1
N4BiasFieldCorrection -i t1.nii.gz -o t1_unb.nii.gz
#bet t1_unb t1_unb_bet -m -f 0.4
python3 /mnt/data/04_CodeBase/synthstrip-docker -i t1_unb.nii.gz -o t1_unb_bet.nii.gz -m t1_unb_bet_mask.nii.gz

cd ../T2
#bet t2 t2_bet -m -f 0.4
python3 /mnt/data/04_CodeBase/synthstrip-docker -i t2.nii.gz -o t2_bet.nii.gz -m t2_bet_mask.nii.gz
cd ..

############ if using bet CHECK RESULTS #####################

# To obtain a good alignment, perform bbr with FSL fast. The WM mask is t1_unb_bet_pve_2.nii.gz
cd T1
fast t1_unb_bet
cd ../DWI
flirt -in b0_preproc_bet -ref ../$T1/t1_unb_bet -dof 6 -omat diff2struct_fsl_initial.mat
flirt -in b0_preproc_bet -ref ../$T1/t1_unb_bet -dof 6 -cost bbr -wmseg ../$T1/t1_unb_bet_pve_2 -init diff2struct_fsl_initial.mat \
-omat diff2struct_fsl_bbr.mat -schedule /usr/local/fsl/etc/flirtsch/bbr.sch
transformconvert diff2struct_fsl_bbr.mat b0_preproc_bet.nii.gz ../$T1/t1_unb_bet.nii.gz flirt_import diff2struct_bbr_mrtrix.txt
mrtransform ../$T1/t1_unb.nii.gz -linear diff2struct_bbr_mrtrix.txt -inverse t1_coreg.mif

#Transform nearest neighbour (use with parcellation/atlas images)
flirt -in b0_preproc_bet -ref ../$T1/t1_unb_bet -dof 6 -cost bbr -wmseg ../$T1/t1_unb_bet_pve_2 -init diff2struct_fsl_initial.mat \
-omat diff2struct_fsl_bbr_nearest.mat -schedule /usr/local/fsl/etc/flirtsch/bbr.sch -interp nearestneighbour
transformconvert diff2struct_fsl_bbr_nearest.mat b0_preproc_bet.nii.gz ../$T1/t1_unb_bet.nii.gz flirt_import diff2struct_bbr_nearest_mrtrix.txt

# Transform T2
flirt -in b0_preproc_bet -ref ../T2/t2_bet -dof 6 -omat diff2structT2_fsl.mat
transformconvert diff2structT2_fsl.mat b0_preproc_bet.nii.gz ../T2/t2_bet.nii.gz flirt_import diff2structT2_mrtrix.txt
mrtransform ../T2/t2.nii.gz -linear diff2structT2_mrtrix.txt -inverse t2_coreg.mif