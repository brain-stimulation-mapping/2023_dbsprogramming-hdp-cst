#!/bin/bash
# Alba Segura Amil, october '22

# Script to coregister T1 to MNI-FSL space.
# Data will be saved in /subjects/subjectID/atlasRegistration_fsl

## ----------------------------------------
# Inputs:
# - path to subjects folder: $path
# - subject ID in subjects folder: $pat
# - Patient T1: t1_unb.nii.gz
# - Patient T1 bet: t1_unb_bet.nii.gz
# - MNI-FSL T1: MNI152_T1_1mm_brain.nii.gz

## Outputs in atlasRegistration_fsl/:
# - ants1Warp.nii.gz
# - ants0GenericAffine.mat
# - mrtrix_warp[].nii
# - mrtrix_warp_corrected.mif & mrtrix_warp_corrected_inv.mif
# - T1inMNI.mif
# - mrtrix_warp_corrected_inv.mif

##------------------------------------------

path="/home/alba/2021_HDP/03_Data/subjects" # path to subjects folder
pat = '' # patID in subjects folder
T1_pat="$path/$pat/T1/t1_unb.nii.gz" # Patient T1
T1_pat_bet="$path/$pat/T1/t1_unb_bet.nii.gz" # Patient T1 bet
T1_MNI="/home/alba/03_Data/MNI_fsl/T1_1mm/MNI152_T1_1mm_brain.nii.gz" # FSL-MNI T1

cd $path/$pat
mkdir atlasRegistration_fsl # folder to save transformations
cd atlasRegistration_fsl

# ANTS transformation
antsRegistration --verbose 1 --dimensionality 3 --float 0 --output [ants,antsWarped.nii.gz,antsInverseWarped.nii.gz] \
--interpolation Linear --use-histogram-matching 1 --winsorize-image-intensities [0.005,0.995] \
--transform Rigid[0.1] --metric CC[$T1_MNI,$T1_pat_bet,1,4,Regular,0.1] --convergence [1000x500x250x100,1e-6,10] --shrink-factors 8x4x2x1 --smoothing-sigmas 3x2x1x0vox \
--transform Affine[0.1] --metric CC[$T1_MNI,$T1_pat_bet,1,4,Regular,0.2] --convergence [1000x500x250x100,1e-6,10] --shrink-factors 8x4x2x1 --smoothing-sigmas 3x2x1x0vox \
--transform SyN[0.1,3,0] --metric CC[$T1_MNI,$T1_pat_bet,1,4] --convergence [100x70x50x20,1e-6,10] --shrink-factors 4x2x2x1 --smoothing-sigmas 2x2x1x0vox

# Convert transform for use in mrtrix
mrconvert $T1_pat_bet T1_bet.mif
warpinit T1_bet.mif identity_warp[].nii
for i in {0..2};
do antsApplyTransforms -d 3 -e 0 -i identity_warp${i}.nii -o mrtrix_warp${i}.nii -r $T1_MNI -t ants1Warp.nii.gz -t ants0GenericAffine.mat --default-value 2147483647;
done
warpcorrect mrtrix_warp[].nii mrtrix_warp_corrected.mif -marker 2147483647

# Apply transform to T1 to visualize result of co-registration
mrtransform $T1_pat -warp mrtrix_warp_corrected.mif T1inMNI.mif

# If visual inspection is good, inverted warp will be also correct
warpinvert mrtrix_warp_corrected.mif mrtrix_warp_corrected_inv.mif


#fsl
#convert_xfm -omat 0_flirt/structT2_to_diff.mat -inverse ../DQS/diff2structT2_fsl.mat
#flirt -in t2_bet -ref ../DQS/b0_preproc_bet -out 0_flirt/t2_indiff_refb0 -init 0_flirt/structT2_to_diff.mat -applyxfm
#flirt -in t2_bet -ref t2_bet -out 0_flirt/t2_indiff_refT2 -init 0_flirt/structT2_to_diff.mat -applyxfm
#convert_xfm -omat structT2_to_diff.mat -inverse diff2structT2_fsl.mat
