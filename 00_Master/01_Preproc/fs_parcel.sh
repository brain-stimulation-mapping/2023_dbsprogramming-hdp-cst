#!/bin/bash
# Alba Segura Amil, october '22

# Script performs Freesurfer cortical parcellation (recon-all) including additional Brodmann areas parcels.
# Extraction of Brodmann areas of interest.

## ----------------------------------------
## Inputs:
# - path to subjects folder: $path
# - subject ID in subjects folder: $pat
# - Fressurfer subject ID: $subjectID
# - path to folder containing LUT (BA_exth_original.txt & BA_exth_ordered.txt): $LUT_path
# - structural MRI: t1.nii.gz & t2.nii.gz
# - diffusion to structural transformation: diff2struct_bbr_nearest_mrtrix.txt

## Outputs:
# - BA_exth.mgz
# - BA_exth_parcels.mif
# - BA_exth_parcels_coreg.mif
# - R_BAx.mif & L_BAx.mif

#TODO
# if subjects ID would be the same in the freesurfer/recon-all folder and in the data/subjects folder,
# $pat & $subjectID could be the same variable

##------------------------------------------
path="/mnt/data/2020_Projects/2021_HDP/03_Data/subjects" # path to subjects folder
pat='' # patID in subjects folder
LUT_path="/home/alba/03_Data/fs_LUT/" # path to folder containing LUT
subjectID='' # freesurfer subject ID

cd $path/$pat

## If only T1 available
#recon-all -s $subjectID -i T1/t1.nii.gz -all -parallel

## If T2 available
#recon-all -s $subjectID -i T1/t1.nii.gz -T2 T2/t2.nii.gz -all -parallel

## Parcellation Brodmann areas (BA)
mkdir fs_parcellation
mkdir fs_parcellation/brodmann
cd fs_parcellation/brodmann

# generate freesurfer BA parcellation
mri_aparc2aseg --old-ribbon --s $subjectID --annot BA_exvivo.thresh --o BA_exth.mgz
# re-index BA parcellation for use in mrtrix
labelconvert BA_exth.mgz $LUT_path/BA_exth_original.txt $LUT_path/BA_exth_ordered.txt BA_exth_parcels.mif

# transform BA parcellation from T1 space to diffusion space
mrtransform BA_exth_parcels.mif -linear $path/$pat/DWI/diff2struct_bbr_nearest_mrtrix.txt -inverse BA_exth_parcels_coreg.mif

## Extract BA of interest (index correspond to the assigned values in BA_exth_ordered.txt)
#mrcalc BA_exth_parcels_coreg.mif 1 -eq L_BA1.mif
#mrcalc BA_exth_parcels_coreg.mif 2 -eq L_BA2.mif
#mrcalc BA_exth_parcels_coreg.mif 3 -eq BA_exth_parcels_coreg.mif 4 -eq -add L_BA3.mif
mrcalc BA_exth_parcels_coreg.mif 5 -eq BA_exth_parcels_coreg.mif 6 -eq -add L_BA4.mif
mrcalc BA_exth_parcels_coreg.mif 7 -eq L_BA6.mif
mrcalc L_BA4.mif L_BA6.mif -max L_BA4_BA6.mif

#mrcalc BA_exth_parcels_coreg.mif 15 -eq R_BA1.mif
#mrcalc BA_exth_parcels_coreg.mif 16 -eq R_BA2.mif
#mrcalc BA_exth_parcels_coreg.mif 17 -eq BA_exth_parcels_coreg.mif 18 -eq -add R_BA3.mif;
mrcalc BA_exth_parcels_coreg.mif 19 -eq BA_exth_parcels_coreg.mif 20 -eq -add R_BA4.mif
mrcalc BA_exth_parcels_coreg.mif 21 -eq R_BA6.mif
mrcalc R_BA4.mif R_BA6.mif -max R_BA4_BA6.mif