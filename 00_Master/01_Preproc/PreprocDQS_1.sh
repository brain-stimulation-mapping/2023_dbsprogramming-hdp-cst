#!/bin/bash
# Alba Segura Amil, october '22

# Script for preprocessing.sh DQS scans without reversed PE (part 1).

## ----------------------------------------
## Inputs:
# - path to subjects folder: $path
# - subject ID in subjects' folder: $pat
# - diffusion MRI data: dwi_raw.mif

## Outputs:
# - dwi_den.mif & dwi_den_unr.mif
# - 00_synb0/OUTPUTS/
# - 00_eddy/b0_bet.nii.gz ----------> IMPORTANT check accuracy of bet before continuing to the next step

#TODO
# bet could be replaced by the new brain extraction tool from Freesurfer. Accuracy seems to be better.

##------------------------------------------
# path to subjects folder
path="/mnt/data/2020_Projects/2021_HDP/03_Data"
# patID in subjects folder
pat='subjects/xxxx'

cd $path/$pat/DWI

# Denoising and unringing
dwidenoise dwi_raw.mif dwi_den.mif
mrdegibbs dwi_den.mif dwi_den_unr.mif -axes '0,1'

# Prepare data and run synb0 to generate input for FSL topup
cp -r $path/00_synb0 $path/$pat/DWI  #00_synb0 folder contains INPUTS/OUTPUTS dir and acqparams.txt file necessary to run synb0
dwiextract dwi_den_unr.mif 00_synb0/INPUTS/b0.nii.gz -bzero
cp $path/$pat/T1/t1.nii.gz 00_synb0/INPUTS/T1.nii.gz

cd 00_synb0
docker run --rm \
  -v $(pwd)/INPUTS/:/INPUTS/ \
  -v $(pwd)/OUTPUTS:/OUTPUTS/ \
  -v /usr/local/freesurfer/license.txt:/extra/freesurfer/license.txt \
  --user $(id -u):$(id -g) \
  justinblaber/synb0_25iso
cd ..

# Prepare data for eddy
cp -r $path/00_eddy $path/$pat/DWI  #00_eddy folder contains acparams.txt and index.txt files necessary to run eddy
mrconvert dwi_den_unr.mif 00_eddy/dwi_den_unr.nii.gz -export_grad_fsl 00_eddy/bvecs 00_eddy/bvals
bet 00_synb0/INPUTS/b0.nii.gz 00_eddy/b0_bet -m -f 0.2