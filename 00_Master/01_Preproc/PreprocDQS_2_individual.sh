#!/bin/bash
# Alba Segura Amil, october '22

# Script for preprocessing.sh DQS scans without reversed PE (part 2) for an individual subject.
# Eddy, DWI mask, response function, fiber orientation distribution, normalization.

# Requirements: Run PreprocDQS_1 and visually inspect result from bet
# If the analysis is done for a group of subjects, run Preproc_DQS_2_group.sh instead !!!!

## ----------------------------------------
# Inputs:
# - path to subjects folder: $path
# - subject ID in subjects' folder: $pat
# - 00_synb0/OUTPUTS
# - data prepared in 00_eddy/

## Outputs:
# - dwi_preproc.mif
# - mask_preproc.mif
# - wm.txt gm.txt csf.txt
# - mfod.mif gm.txt gmfod.mif csffod.mif
# - wmfod_norm.mif gmfod_norm.mif csffod_norm.mif

##------------------------------------------
# path to subjects folder
path="/mnt/data/2020_Projects/2021_HDP/03_Data"
# patID in subjects' folder
pat='subjects/xxxx'

# Run eddy
cd $path/$pat/DWI/00_eddy
eddy_openmp --imain=dwi_den_unr.nii.gz --mask=b0_bet_mask.nii.gz --acqp=acqparams.txt --index=index.txt --bvecs=bvecs --bvals=bvals \
--topup=../00_synb0/OUTPUTS/topup --out=eddy_unwarped_images --data_is_shelled --repol --cnr_maps
mrconvert eddy_unwarped_images.nii.gz ../dwi_preproc.mif -fslgrad eddy_unwarped_images.eddy_rotated_bvecs bvals
cd ..

## Mask estimation
#dwibiascorrect ants dqs_preproc.mif dqs_preproc_unb.mif -bias bias.mif  # not necessary as long as mtnormalise is used
dwi2mask dwi_preproc.mif mask_preproc.mif

############### If analysis for a group of subjects, PreprocDQS_2_group.sh instead !!!!

# Response function estimation
dwi2response dhollander dwi_preproc.mif wm.txt gm.txt csf.txt -voxels voxels.mif

## When using multi-shell data
# Fiber orientation distribution
dwi2fod msmt_csd dwi_preproc.mif -mask mask_preproc.mif wm.txt wmfod.mif gm.txt gmfod.mif csf.txt csffod.mif
# Instensity normalization
mtnormalise wmfod.mif wmfod_norm.mif gmfod.mif gmfod_norm.mif csffod.mif csffod_norm.mif -mask mask_preproc.mif

# When using single shell data
#dwi2fod msmt_csd dwi_preproc.mif -mask mask_preproc.mif wm.txt wmfod.mif csf.txt csffod.mif
#mtnormalise wmfod.mif wmfod_norm.mif csffod.mif csffod_norm.mif -mask mask_preproc.mif