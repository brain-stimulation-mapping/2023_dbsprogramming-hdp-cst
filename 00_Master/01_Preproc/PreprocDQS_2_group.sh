#!/bin/bash
# Alba Segura Amil, october '22

# Script for preprocessing.sh DQS scans without reversed PE (part 2) for a group of subjects.
# Runs eddy

# Requirements: Run PreprocDQS_1 and visually inspect result from bet

## ----------------------------------------
# Inputs:
# - path to subjects folder: $path
# - subject ID in subjects' folder: $pat
# - 00_synb0/OUTPUTS
# - data prepared in 00_eddy/

## Outputs:
# - dwi_preproc.mif

##------------------------------------------
# path to subjects folder
path="/mnt/data/2020_Projects/2021_HDP/03_Data"
# patID in subjects' folder
pat='subjects/xxxx'

# Run eddy
cd $path/$pat/DWI/00_eddy
eddy_openmp --imain=dwi_den_unr.nii.gz --mask=b0_bet_mask.nii.gz --acqp=acqparams.txt --index=index.txt --bvecs=bvecs --bvals=bvals \
--topup=../00_synb0/OUTPUTS/topup --out=eddy_unwarped_images --data_is_shelled --repol --cnr_maps
mrconvert eddy_unwarped_images.nii.gz ../dwi_preproc.mif -fslgrad eddy_unwarped_images.eddy_rotated_bvecs bvals