#!/bin/bash

### CST tracking **group** with inclusion ROI for CST and exclusion ML

path="/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/"
CST="00_CST_5"
cd $path

# Get medial CST ROI from JHU atlas (previously coregistered in 'CSTtracking')
for_each */atlasRegistration_fsl/00_JHU : mrcalc IN/JHU-ICBM-labels-1mm_coreg.nii.gz 7 -eq IN/R_cst.mif
for_each */atlasRegistration_fsl/00_JHU : mrcalc IN/JHU-ICBM-labels-1mm_coreg.nii.gz 8 -eq IN/L_cst.mif

# Get medial lemniscus ROI from JHU atlas (previously coregistered in 'CSTtracking')
for_each */atlasRegistration_fsl/00_JHU : mrcalc IN/JHU-ICBM-labels-1mm_coreg.nii.gz 9 -eq IN/R_ml.mif
for_each */atlasRegistration_fsl/00_JHU : mrcalc IN/JHU-ICBM-labels-1mm_coreg.nii.gz 10 -eq IN/L_ml.mif

for_each * : mkdir IN/tractography/$CST  # new directory to save CST tracts output

# Tracking CST
for_each * : tckedit IN/tractography/00_CST/L_CST.tck IN/tractography/$CST/L_CST.tck \
-tck_weights_in IN/tractography/00_CST/L_CST_weights.csv -tck_weights_out IN/tractography/$CST/L_CST_weights.csv \
-include IN/atlasRegistration_fsl/00_JHU/L_cst.mif \
-exclude IN/atlasRegistration_fsl/00_JHU/L_ml.mif \
-exclude IN/atlasRegistration_fsl/00_JHU/R_cst.mif \
-exclude IN/atlasRegistration_fsl/00_JHU/R_ml.mif

for_each * : tckedit IN/tractography/00_CST/R_CST.tck IN/tractography/$CST/R_CST.tck \
-tck_weights_in IN/tractography/00_CST/R_CST_weights.csv -tck_weights_out IN/tractography/$CST/R_CST_weights.csv \
-include IN/atlasRegistration_fsl/00_JHU/R_cst.mif \
-exclude IN/atlasRegistration_fsl/00_JHU/R_ml.mif \
-exclude IN/atlasRegistration_fsl/00_JHU/L_cst.mif \
-exclude IN/atlasRegistration_fsl/00_JHU/L_ml.mif -force

