#!/bin/bash

### CST tracking **group** with additional exclusion ROI for medial lemniscus

path="/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/"
CST="00_CST_2"
cd $path

# Get medial lemniscus ROI from JHU atlas (previously coregistered in 'CSTtracking')
for_each */atlasRegistration_fsl/00_JHU : mrcalc IN/JHU-ICBM-labels-1mm_coreg.nii.gz 9 -eq IN/R_ml.mif
for_each */atlasRegistration_fsl/00_JHU : mrcalc IN/JHU-ICBM-labels-1mm_coreg.nii.gz 10 -eq IN/L_ml.mif

for_each * : mkdir IN/tractography/$CST  # new directory to save CST tracts output

# Tracking CST
for_each * : tckedit IN/tractography/tracks_10mio.tck IN/tractography/$CST/L_CST.tck -minlength 80 -maxlength 135 \
-include IN/fs_parcellation/brodmann/L_BA4_BA6.mif \
-include IN/atlasRegistration_fsl/00_JHU/L_supCorRad.mif -include IN/atlasRegistration_fsl/00_JHU/L_postLimbIC.mif \
-include IN/atlasRegistration_fsl/00_JHU/L_ped.mif  -include IN/fs_parcellation/brainstem/medulla.mif \
-exclude IN/atlasRegistration_fsl/00_JHU/CC.mif -exclude IN/atlasRegistration_fsl/00_JHU/L_ml.mif \
-exclude IN/fs_parcellation/desikan/L_cerebellum.mif -exclude IN/fs_parcellation/desikan/R_cerebellum.mif  \
-tck_weights_in IN/tractography/sift2_weights_10mio.csv -tck_weights_out IN/tractography/$CST/L_CST_weights.csv

for_each * : tckedit IN/tractography/tracks_10mio.tck IN/tractography/$CST/R_CST.tck -minlength 80 -maxlength 135 \
-include IN/fs_parcellation/brodmann/R_BA4_BA6.mif \
-include IN/atlasRegistration_fsl/00_JHU/R_supCorRad.mif -include IN/atlasRegistration_fsl/00_JHU/R_postLimbIC.mif \
-include IN/atlasRegistration_fsl/00_JHU/R_ped.mif  -include IN/fs_parcellation/brainstem/medulla.mif \
-exclude IN/atlasRegistration_fsl/00_JHU/CC.mif -exclude IN/atlasRegistration_fsl/00_JHU/R_ml.mif \
-exclude IN/fs_parcellation/desikan/L_cerebellum.mif -exclude IN/fs_parcellation/desikan/R_cerebellum.mif \
-tck_weights_in IN/tractography/sift2_weights_10mio.csv -tck_weights_out IN/tractography/$CST/R_CST_weights.csv -force



