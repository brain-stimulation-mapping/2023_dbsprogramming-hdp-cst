# Alba Segura Amil, november '22

# Create dataframes containing sum of weights (HDP and CST) for a group of subjects
# df['LeadID','total_sum_w']

# Inputs:
# - path to subjects folder: rootdir
# - directory to save data: dirsave

# Outputs:
# - df_total_sum_w_HDP.csv
# - df_total_sum_w_CST_3.csv

from pathlib import Path
from collections import namedtuple
import numpy as np
import pandas as pd

rootdir = '/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/'
dir_save = '/mnt/data/2020_Projects/2021_HDP/03_Data/FBCcalc_simbio_fastfield_20subj/'

p = Path(rootdir)

File_HDP = namedtuple('File', 'name total_sum_w path')
File_CST = namedtuple('File', 'name total_sum_w path')

# create empty list
files_HDP = []
files_CST = []

# loop in "subjects" folder to repeat process for each subject
# LOAD DATA FROM .csv FILES
for item in sorted(p.glob('**/tractography/*_STN_BA_90_weights.csv')):
    name = item.name
    path = Path.resolve(item).parent
    weights = np.genfromtxt(item, delimiter=' ')
    sum_weights = np.sum(weights)
    files_HDP.append(File_HDP(name, sum_weights, path))

df_HDP = pd.DataFrame(files_HDP)
# create LeadID column with PatientID and hemisphere
df_HDP['LeadID'] = pd.DataFrame((df_HDP.path.apply(str).str.split('/').str[7]) + '_' + df_HDP.name.str.split('_').str[0])
df_HDP = df_HDP[['LeadID', 'total_sum_w']]

for item in sorted(p.glob('**/tractography/00_CST_5/*CST_weights.csv')):
    name = item.name
    path = Path.resolve(item).parent
    weights = np.genfromtxt(item, delimiter=' ')
    sum_weights = np.sum(weights)
    files_CST.append(File_CST(name, sum_weights, path))

df_CST = pd.DataFrame(files_CST)
df_CST['LeadID'] = pd.DataFrame((df_CST.path.apply(str).str.split('/').str[7]) + '_' + df_CST.name.str.split('_').str[0])
df_CST = df_CST[['LeadID', 'total_sum_w']]

# Save data
df_HDP.to_csv(dir_save + 'df_total_sum_w_HDP.csv', index=False)
df_CST.to_csv(dir_save + 'df_total_sum_w_CST_3.csv', index=False)