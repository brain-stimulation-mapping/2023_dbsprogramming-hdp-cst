# Alba Segura Amil, november '22

# Group script to create, for each subject, dataframes for effect and side_effect containing the following data:
# ['contact', 'name', 'count', 'total_count', 'percent', 'sum_w', 'total_sum_w', 'percent_w', 'fbc', 'path']

# Requierements:
# - df_total_sum_w_HDP.csv & df_total_sum_w_CST_3.csv (df generated in total_sum_w.py)
# - /tractography/mu.txt (subject's mu parameter, obtained in 17b)
# - tracks_vat_L_STN_BA_90_weights.csv & tracks_vat_R_STN_BA_90_weights.csv (HDP tracts inside VTAs, obtained in 22)
# - tracks_vat_L_CST_weights.csv & tracks_vat_R_CST_weights.csv (CST tracts inside VTAs, obtained in 22)

# Inputs:
# - path to subjects folder: rootdir
# - directory to save data: dirsave

# Outputs:
# - data_HDP_VTAmassive.csv
# - data_CST_VTAmassive.csv


from pathlib import Path
import os
from collections import namedtuple
import numpy as np
import pandas as pd
import re

######### modify dir, dir_save and name to save
filename_HDP = 'data_HDP_VTAmassive.csv'  # name of the file to save
filename_CST = 'data_CST_VTAmassive.csv'  # name of the file to save
dir_save = '/mnt/data/2020_Projects/2021_HDP/03_Data/FBCcalc_simbio_fastfield_20subj/'
#############################################################

#################### load data total_sum_weights for each pathway --> dataframes created in 'total_sum_w.py'
sum_w_CST = pd.read_csv(dir_save + 'df_total_sum_w_CST_3.csv')
sum_w_HDP = pd.read_csv(dir_save + 'df_total_sum_w_HDP.csv')
#############################

# Main dir containing all subjects
rootdir = Path("/mnt/data/2020_Projects/2021_HDP/03_Data/PatientVTAs/03_Data/")

#Create files namedtuple
File = namedtuple('File', 'name sum_w fbc path')
FileS = namedtuple('FileS', 'nameS count total_count percent pathS')

#Create empty lists
files = []
filesS = []

#for dir in sorted(rootdir.glob('*')):
for dir in sorted(rootdir.iterdir()):

    subject = os.path.basename(dir)
    subj_dir = Path(dir)
    print(subject)
    mu = np.genfromtxt('/mnt/data/2020_Projects/2021_HDP/03_Data/subjects/' + subject + '/tractography/mu.txt') #subject's mu parameter

    for item in sorted(subj_dir.glob('**/*')):
        #print(item)
        if item.name in ['tracks_vat_L_STN_BA_90_weights.csv'] or item.name in ['tracks_vat_R_STN_BA_90_weights.csv'] \
                or item.name in ['tracks_vat_L_CST_weights.csv'] or item.name in ['tracks_vat_R_CST_weights.csv']:
            name = item.name
            path = Path.resolve(item).parent
            weights = np.genfromtxt(item, delimiter=' ')
            sum_w = np.sum(weights)
            fbc = mu * sum_w
            files.append(File(name, sum_w, fbc, path))

        if item.name in ['tracks_vat_L_STN_BA_90.tck'] or item.name in ['tracks_vat_R_STN_BA_90.tck'] \
                or item.name in ['tracks_vat_L_CST.tck'] or item.name in ['tracks_vat_R_CST.tck']:
            nameS = item.name
            pathS = Path.resolve(item).parent
            # Open the and read .tck file (header + binary file --> 'rb')
            file = open(item, 'rb')
            content = file.readlines()
            # Get values of count and tot_count
            stream_count = re.findall('count: \d+', str(content[:]))  # retrieve values for count and total_count
            count = re.findall('\d+', str(stream_count[0]))  # retrieve numerical value for count
            count = int(count[0])
            total_count = re.findall('\d+', str(stream_count[1]))  # retrieve numerical value for total_count
            total_count = int(total_count[0])
            # Calculate activation percentage
            percent = count * 100 / total_count
            filesS.append(FileS(nameS, count, total_count, percent, pathS))


# Create dataframes from lists
df1 = pd.DataFrame(files)  # dataframe with data from .csv files --> weights
df2 = pd.DataFrame(filesS)  # dataframe with data from .tck files --> streamlines
df = pd.concat([df1['name'], df2['count'], df2['total_count'], df2['percent'], df1['sum_w'],
                df1['fbc'], df1['path']], axis=1)  # concatenate both dataframes

# Add 'contact' column
df['path'] = pd.DataFrame(df.path.astype(str))
df['contact'] = pd.DataFrame(df.path.str.split('/').str[11])

# Add new column with lead L/R
df['lead'] = pd.DataFrame(df.name.str.split('_').str[2])  # split entry in substrings and keep the one in position 2

# Create empty column
df['C'] = ""
maskL = (df['lead'] == 'L')  # if 'L' in column lead
df['C'][maskL] = (df.contact.str.split('-|_').str[0])  # keep contactID in first position [0] --> left contact)
maskR = (df['lead'] == 'R')  # if 'R' in column lead
df['C'][maskR] = (df.contact.str.split('-|_').str[1])  # keep contactID in second position [1] --> right contact)

# Keep only numerical values in column C
df['C'] = df['C'].replace('e', '', regex=True)

# Add new columns with LeadID & ContactID
df['LeadID'] = pd.DataFrame((df.path.str.split('/').str[8]).map(str) + '_' + (df.name.str.split('_').str[2]).map(str))
df['ContactID'] = pd.DataFrame((df.path.str.split('/').str[8]).map(str) + '_' + (df.C))

# Add new column with mA
df['mA'] = pd.DataFrame(df.contact.str.split('_').str[1])

# Add total_sum_w column
# from total_sum_w  --> dataframes created in 'total_sum_w.py'
# split df according to pathway
df_HDP = df[df['name'].str.contains('STN')]
df_CST = df[df['name'].str.contains('CST')]

# extract colums as lists and create dictionaries
LeadID_HDP = sum_w_HDP['LeadID'].to_list()
total_sum_w_HDP = sum_w_HDP['total_sum_w'].to_list()
dict_sum_w_HDP = dict(zip(LeadID_HDP, total_sum_w_HDP))

LeadID_CST = sum_w_CST['LeadID'].to_list()
total_sum_w_CST = sum_w_CST['total_sum_w'].to_list()
dict_sum_w_CST = dict(zip(LeadID_CST, total_sum_w_CST))

# use dictionaries for creating new column total_sum_w and percent_w
df_HDP['total_sum_w'] = df_HDP['LeadID'].map(dict_sum_w_HDP)
df_HDP['percent_w'] = df_HDP['sum_w']*100/df_HDP['total_sum_w']

df_CST['total_sum_w'] = df_CST['LeadID'].map(dict_sum_w_CST)
df_CST['percent_w'] = df_CST['sum_w']*100/df_CST['total_sum_w']

# Keep columns of interest
data_HDP = df_HDP[['LeadID', 'ContactID', 'mA',  'count', 'total_count', 'percent', 'sum_w', 'total_sum_w', 'percent_w', 'fbc']]
data_CST = df_CST[['LeadID', 'ContactID', 'mA',  'count', 'total_count', 'percent', 'sum_w', 'total_sum_w', 'percent_w', 'fbc']]

# Save data
data_HDP.to_csv(dir_save + filename_HDP, index=False)
data_CST.to_csv(dir_save + filename_CST, index=False)