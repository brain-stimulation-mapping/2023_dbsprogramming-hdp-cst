# Get streamlines count from .tck file
import re

file2read = '/home/alba/2021_HDP/03_Data/subjects/Pat_WiDa/stimulations_all_simbio/MNI_ICBM_2009b_NLIN_ASYM/e6-14/tracks_vat_L_STN_BA_90.tck'

# Open the and read .tck file (header + binary file --> 'rb')
file = open(file2read, 'rb')
content = file.readlines()
# Get values of count and tot_count

stream_count = re.findall('count: \d+', str(content[:])) #retrieve values for count and total_count
count = re.findall('\d+', str(stream_count[0])) #retrieve numerical value for count

#tot_count = re.findall('total_count: \d+', str(content[:]))
total_count = re.findall('\d+', str(stream_count[1])) #retrieve numerical value for total_count

#Calculate activation percentage
percent = int(count[0])*100/int(total_count[0])
print(percent)
