#!/bin/bash
# convert .mif files to .nii.gz to reduce file size

path="/mnt/data/2020_Projects/2021_HDP/03_Data/PatientVTAs/03_Data"

cd $path

for dir in * ; do
    echo "$dir"
    dir="$dir"

    for_each $dir/stimulations/MNI_ICBM_2009b_NLIN_ASYM/* : mrconvert IN/vat_left_coreg.mif IN/vat_left_coreg.nii.gz -force
    #for_each $dir/stimulations/MNI_ICBM_2009b_NLIN_ASYM/* : rm IN/vat_left_coreg.mif

    for_each $dir/stimulations/MNI_ICBM_2009b_NLIN_ASYM/* : mrconvert IN/vat_right_coreg.mif IN/vat_right_coreg.nii.gz -force
    #for_each $dir/stimulations/MNI_ICBM_2009b_NLIN_ASYM/* : rm IN/vat_right_coreg.mif

done
