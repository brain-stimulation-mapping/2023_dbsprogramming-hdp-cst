import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
import pandas as pd

dir_save = '/mnt/data/2020_Projects/2021_HDP/03_Data/FBCcalc_simbio_fastfield_20subj/'

df = pd.read_csv('/mnt/data/2020_Projects/2021_HDP/03_Data/FBCcalc_simbio_fastfield_20subj/df_pathways_CST2.csv')

data_HDP = df[df['pathway'] == 'HDP']
data_CST = df[df['pathway'] == 'CST_2']

# Logistic regression
x_HDP = np.array(data_HDP['percent_w'])
y_HDP = np.array(data_HDP['outcome'])

x_CST = np.array(data_CST['percent_w'])
y_CST = np.array(data_CST['outcome'])

# Load data
HDP = pd.read_csv(dir_save + 'df_HDP_sorted_test.csv')
CST = pd.read_csv(dir_save + 'df_CST_sorted_test.csv')

ax = plt.subplot()

#HDP.plot(x='x', y='pred_test', ax=ax, color='C0', label='HDP')
#CST.plot(x='x', y='pred_test', ax=ax, color='orange', label='CST')
HDP.plot(x='x_HDP', y='y_HDP', ax=ax, color='C0', label='HDP')
CST.plot(x='x_CST', y='y_CST', ax=ax, color='orange', label='CST')
plt.xscale('log')
plt.axhline(y=0.5, color='grey', linestyle='dashed')

#plt.scatter(X.ravel(), y, marker='.');
plt.scatter(x_HDP, y_HDP, color='C0', marker='o', alpha=0.1, )
plt.scatter(x_CST, y_CST, color='orange', marker='o', alpha=0.1)

plt.xlabel ('Percent weights')
plt.ylabel ('Outcome')

plt.show()
